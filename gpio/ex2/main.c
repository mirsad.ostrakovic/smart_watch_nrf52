#include <nrf52.h>
#include "gpio.h"
#include "gpiote.h"
#include "timer.h"
#include "ppi.h"



void toggle_LED1()
{
	static char state = 0;

	if(state == 0)
	{
		turnOFF_LED1();
		state = 1;	
	}
	else 
	{
		turnON_LED1();
		state = 0;	
	}
}


void TIMER1_message()
{
	//sprintUART0("TIMER1_Message(): called\r\n");
	
	// Manual generate event for GPIOTE peripheral, which will toggle pin value	
	// NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL] = 1;
}


int main(void)
{
	
		init_TIMER1();	
		init_LED1();  
		init_LED1_GPIOTE(); 


		//set_TIMER1_IRQ_callback(TIMER1_message);

		// Set PPI(Programmable peripheral interconnection) to connect TIMER1 COMPARE[0] event to NRF_GPIOTE TASK_OUT for LED1_CHANNEL
		configure_PPI(NRF52832_PPI_CHANNEL_0, (uint32_t)&(NRF_TIMER1->EVENTS_COMPARE[0]), (uint32_t)&(NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL]));

		while (1);

}


