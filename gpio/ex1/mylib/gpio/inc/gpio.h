#ifndef GPIO_H_
#define GPIO_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>


#define NRF52832_GPIO_PIN0 							0
#define NRF52832_GPIO_PIN1 							1
#define NRF52832_GPIO_PIN2 							2
#define NRF52832_GPIO_PIN3 							3
#define NRF52832_GPIO_PIN4 							4	
#define NRF52832_GPIO_PIN5 							5
#define NRF52832_GPIO_PIN6 							6
#define NRF52832_GPIO_PIN7 							7
#define NRF52832_GPIO_PIN8 							8
#define NRF52832_GPIO_PIN9 							9
#define NRF52832_GPIO_PIN10 						10
#define NRF52832_GPIO_PIN11 						11
#define NRF52832_GPIO_PIN12 						12
#define NRF52832_GPIO_PIN13 						13
#define NRF52832_GPIO_PIN14 						14
#define NRF52832_GPIO_PIN15 						15
#define NRF52832_GPIO_PIN16 						16
#define NRF52832_GPIO_PIN17 						17
#define NRF52832_GPIO_PIN18 						18
#define NRF52832_GPIO_PIN19 						19
#define NRF52832_GPIO_PIN20 						20
#define NRF52832_GPIO_PIN21 						21
#define NRF52832_GPIO_PIN22 						22
#define NRF52832_GPIO_PIN23 						23
#define NRF52832_GPIO_PIN24 						24
#define NRF52832_GPIO_PIN25 						25
#define NRF52832_GPIO_PIN26 						26
#define NRF52832_GPIO_PIN27 						27
#define NRF52832_GPIO_PIN28 						28
#define NRF52832_GPIO_PIN29 						29
#define NRF52832_GPIO_PIN30 						30
#define NRF52832_GPIO_PIN31 						31



// ====================================== LED ============================================
// =======================================================================================

// (nRF52832 Development Kit v1.1.x User Guid v1.2 page 15)
// LED1 => P0.17
// LED2 => P0.18
// LED3 => P0.19
// LED4 => P0.20

#define NRF52832_GPIO_LED1 			NRF52832_GPIO_PIN17 
#define NRF52832_GPIO_LED2			NRF52832_GPIO_PIN18 
#define NRF52832_GPIO_LED3			NRF52832_GPIO_PIN19 
#define NRF52832_GPIO_LED4			NRF52832_GPIO_PIN20 


void init_LED1(); 
void turnON_LED1();
void turnOFF_LED1();

#endif
