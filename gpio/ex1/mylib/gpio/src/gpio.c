#include "gpio.h"
#include "nrf_gpio.h"

#define configure_GPIO(pin, direction, input, pull, drive, sense) \
															do {															  \
																																	\
																NRF_P0->PIN_CNF[pin] = ((((uint32_t)(sense)) << GPIO_PIN_CNF_SENSE_Pos) & GPIO_PIN_CNF_SENSE_Msk) | \
																											 ((((uint32_t)(drive)) << GPIO_PIN_CNF_DRIVE_Pos) & GPIO_PIN_CNF_DRIVE_Msk) | \
																											 ((((uint32_t)(pull)) << GPIO_PIN_CNF_PULL_Pos) & GPIO_PIN_CNF_PULL_Msk) 		| \
																											 ((((uint32_t)(input)) << GPIO_PIN_CNF_INPUT_Pos) & GPIO_PIN_CNF_INPUT_Msk) | \
																											 ((((uint32_t)(direction)) << GPIO_PIN_CNF_DIR_Pos) & GPIO_PIN_CNF_DIR_Msk);  \
															} while(0)





// (nRF52832 Product Specification v1.4, GPIO, page 111)
// See 'struct NRF_GPIO_Type' in file 'nrf52.h' for more information about fields
void init_LED1() 
{

	// (nRF52832 Product Specification v1.4, GPIO, pages 113 and 134)
	// Configure pin as:
	// DIR   <=>  '1'b   pin direction configured as output
	// INPUT <=>  '1'b   disconnected input buffer
	// PULL  <=>  '00'b  no pull
	// DRIVE <=>  '000'b standard '0', standard '1'
	// SENDE <=>  '000'b disabled

	configure_GPIO(NRF52832_GPIO_LED1, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);
}


void turnON_LED1()
{
 //nrf_gpio_pin_set(17);
	// (nRF52832 Product Specification v1.4, GPIO, pages 122)
	// '1'b => pin input is high
	NRF_P0->OUTSET = (1 << NRF52832_GPIO_LED1); 
}



void turnOFF_LED1()
{
	// nrf_gpio_pin_clear(17);
	// (nRF52832 Product Specification v1.4, GPIO, pages 122)
	// '0'b => pin input is high
	NRF_P0->OUTCLR = (1 << NRF52832_GPIO_LED1); 
}


