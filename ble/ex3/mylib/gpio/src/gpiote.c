#include "gpiote.h"


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++ LED ++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ----------------------------------------------------------
// (nRF52832 Development Kit v1.1.x User Guid v1.2 page 17)
// All LEDs are 'active low'
// ----------------------------------------------------------
void init_LED1_GPIOTE() 
{

	// (nRF52832 Product Specification v1.4, GPIO, page 162)
	// Configure GPIOTE as:
	// MODE   	<=> '11'b => Task mode, selected GPIO pin is selected as output, and will perform operation on it's value on SET, CRL or OUT task
	// PSEL   	<=> 'xxxxx'b => GPIO pin selected 
	// POLARITY <=> '11'b => Toggle mode, toggle pin from OUT[n] task
	// OUTINIT  <=> '0'b  => Output init value set as low, so LED1 will initially be TURN ON ()
	configure_GPIOTE(NRF52832_GPIOTE_LED1_CHANNEL, 
									 NRF52832_GPIO_LED1, 
									 GPIOTE_CONFIG_MODE_Task, 
									 GPIOTE_CONFIG_POLARITY_Toggle,
									 GPIOTE_CONFIG_OUTINIT_Low); 
}











// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++ ADXL362 ++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


static uint8_t ADXL362_INT1_flag_ = 0x0;


void init_GPIOTE_ADXL362_INT1_interrupt()
{
	// (nRF52832 Product Specification v1.4, GPIO, page 162)
	// Configure GPIOTE as:
	// MODE   	<=> '01'b => Event mode, selected GPIO pin is selected as input, and the IN[x] event will be generated 
	// 																   if operation specified in POLARITY occurs on the pin
	// PSEL   	<=> 'xxxxx'b => GPIO pin selected 
	// POLARITY <=> '01'b => LoToHi mode, generate IN[x] event when rising edge on the pin occurs
	// OUTINIT  <=> '0'b  => For Event mode(input) this value is not used
	configure_GPIOTE(NRF52832_GPIOTE_ADXL362_INT1_CHANNEL, 
									 NRF52832_GPIO_ADXL362_INT1, 
									 GPIOTE_CONFIG_MODE_Event, 
									 GPIOTE_CONFIG_POLARITY_LoToHi,
									 GPIOTE_CONFIG_OUTINIT_Low); 

	// Clear In Event flag
	NRF_GPIOTE->EVENTS_IN[NRF52832_GPIOTE_ADXL362_INT1_CHANNEL] = 0;

	// (nRF52832 Product Specification v1.4, GPIO, pages 160 and 161)
	// IN[x]  <=>  '1'b => Write '1' enables interrupt for IN[x] event
	NRF_GPIOTE->INTENSET = 1 << NRF52832_GPIOTE_ADXL362_INT1_CHANNEL;


	// Enable interrupt for GPIOTE
	NVIC_ClearPendingIRQ(GPIOTE_IRQn);
	NVIC_SetPriority(GPIOTE_IRQn, 16);
	NVIC_EnableIRQ(GPIOTE_IRQn);
}


uint8_t get_ADXL362_INT1_flag()
{
	return ADXL362_INT1_flag_; 
}



void clear_ADXL362_INT1_flag()
{
	ADXL362_INT1_flag_ = 0x0; 
}










// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++ GPIOTE BASIC +++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


void GPIOTE_IRQHandler()
{

	print("GPIOTE_IRQHandler(): called\r\n");

	// Check is GPIOTE generated as reason of the value change on pin associated with
	// GPIOTE channel for ADXL362 INT 1(aka. is interrupt generated). If it is, then 
	// clear GPIOTE event flag for this event, and set user flag for the same event.
	if(NRF_GPIOTE->EVENTS_IN[NRF52832_GPIOTE_ADXL362_INT1_CHANNEL])
	{
		ADXL362_INT1_flag_ = 0x1; 
		NRF_GPIOTE->EVENTS_IN[NRF52832_GPIOTE_ADXL362_INT1_CHANNEL] = 0;
	}

}


