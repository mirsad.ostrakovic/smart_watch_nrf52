#include "gpio.h"
#include "nrf_gpio.h"




// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++ LED ++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// (nRF52832 Product Specification v1.4, GPIO, page 111)
// See 'struct NRF_GPIO_Type' in file 'nrf52.h' for more information about fields
void init_LED1() 
{

	// (nRF52832 Product Specification v1.4, GPIO, pages 113 and 134)
	// Configure pin as:
	// DIR   <=>  '1'b   pin direction configured as output
	// INPUT <=>  '1'b   disconnected input buffer
	// PULL  <=>  '00'b  no pull
	// DRIVE <=>  '000'b standard '0', standard '1'
	// SENDE <=>  '000'b disabled

	configure_GPIO(NRF52832_GPIO_LED1, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);
}


// ----------------------------------------------------------
// (nRF52832 Development Kit v1.1.x User Guid v1.2 page 17)
// All LEDs are 'active low'
// ----------------------------------------------------------

void turnON_LED1()
{
	// (nRF52832 Product Specification v1.4, GPIO, pages 122)
	// '1'b => pin input is set as high
	NRF_P0->OUTCLR = (1 << NRF52832_GPIO_LED1); 
}



void turnOFF_LED1()
{
	// (nRF52832 Product Specification v1.4, GPIO, pages 122)
	// '1'b => pin input is set as low
	NRF_P0->OUTSET = (1 << NRF52832_GPIO_LED1); 
}











// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++ ADXL362 ++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



void init_ADXL362_INT1_pin()
{
	configure_GPIO(NRF52832_GPIO_ADXL362_INT1,   
								 GPIO_PIN_CNF_DIR_Input,
        				 GPIO_PIN_CNF_INPUT_Connect,
        				 GPIO_PIN_CNF_PULL_Pulldown,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_High);

}
