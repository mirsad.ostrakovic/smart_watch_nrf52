#ifndef DEBUG_H_
#define DEBUG_H_

#include "print.h"

#define DEBUG_IF_BLOCK 0 

#define INIT_DEBUG() 					 				init_PRINT()
#define DEBUG(format, ...) 						print(format, ##__VA_ARGS__)

#define DEBUG_IF(expr, format, ...)			  do{																				\
																								volatile uint32_t expr_val = expr; 	\
																								volatile uint32_t true_val = 1;			\
																																										\
																								if(expr_val) 												\
																									print(format, ##__VA_ARGS__); 		\
																																										\
																									while(true_val);									\
																																										\
																									if(DEBUG_IF_BLOCK)								\
																										while(1);                   		\
																												                        		\
																					}while(1)


#endif
