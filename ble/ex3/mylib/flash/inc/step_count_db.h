#ifndef STEP_COUNT_DB_H_
#define STEP_COUNT_DB_H_

#include "flash.h"


#define	MRV_SUCCESS						0
#define MRV_ERROR_NOT_FOUND		1	

#define USC_SUCCESS															0
#define USC_ERROR_TIMESTAMP_IS_NOT_MOST_RECENT  1
#define USC_WRITE_FLASH_ERROR										2

uint8_t update_step_count(uint32_t step_count_to_add, uint32_t current_timestamp);
uint8_t get_most_recent_step_count(uint32_t *step_count, uint32_t *timestamp, uint32_t current_timestamp);

#endif
