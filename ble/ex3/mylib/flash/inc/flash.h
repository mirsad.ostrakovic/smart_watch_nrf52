#ifndef FLASH_H_
#define FLASH_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include <nrf_soc.h>
#include <ble_srv_common.h>

#include "debug.h"

#define STEP_COUNT_FLASH_SZ 3072
#define USER_INFO_FLASH_SZ  1024

#define FLASH_STEP_COUNT		__attribute__((__section__(".step_count_flash")))
#define FLASH_USER_INFO			__attribute__((__section__(".user_info_flash")))
#define FLASH_MEM_ALIGN			__attribute__((aligned(8)))

extern uint32_t step_count_flash[STEP_COUNT_FLASH_SZ];
extern uint32_t user_info_flash[USER_INFO_FLASH_SZ];


#define FLASH_WAIT_FOR_OP 	0
#define FLASH_OP_SUCCESS		1
#define FLASH_OP_ERROR			2


#define ERASE_FLASH_SUCCESS							0
#define ERASE_FLASH_ERROR_INTERNAL			1
#define ERASE_FLASH_ERROR_INVALID_ADDR	2
#define ERASE_FLASH_ERROR_BUSY					3
#define ERASE_FLASH_ERROR_FORBIDDEN     4
#define ERASE_FLASH_ERROR_FLASH_OP			5
#define ERASE_FLASH_UNKNOWN_ERROR       6


#define WRITE_FLASH_SUCCESS							 0
#define WRITE_FLASH_ERROR_INVALID_ADDR	 1
#define WRITE_FLASH_ERROR_BUSY					 2
#define WRITE_FLASH_ERROR_INVALID_LENGTH 3
#define WRITE_FLASH_ERROR_FORBIDDEN      4
#define WRITE_FLASH_ERROR_FLASH_OP			 5
#define WRITE_FLASH_UNKNOWN_ERROR        6


void flash_sys_event_handler(uint32_t sys_evt);

uint8_t get_flash_state();
uint8_t erase_FLASH(uint32_t page_address, uint8_t is_blocking);
uint8_t write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t sz, uint8_t is_blocking);

/*
void erase_FLASH(uint32_t page_address);
void write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t src_len);
*/

#endif
