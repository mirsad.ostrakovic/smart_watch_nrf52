#include "flash.h"


uint32_t FLASH_STEP_COUNT	FLASH_MEM_ALIGN			step_count_flash[STEP_COUNT_FLASH_SZ];

uint32_t FLASH_USER_INFO  FLASH_MEM_ALIGN			user_info_flash[USER_INFO_FLASH_SZ];

volatile uint8_t flash_state_ = FLASH_WAIT_FOR_OP;






uint8_t get_flash_state()
{
	return flash_state_;
}




void flash_sys_event_handler(uint32_t sys_evt)
{
	switch(sys_evt)
	{
		case NRF_EVT_FLASH_OPERATION_SUCCESS:	
			DEBUG("flash_sys_event_handler(): NRF_EVT_FLASH_OPERATION_SUCCESS\r\n");
			flash_state_ = FLASH_OP_SUCCESS;	 	
			break;

		case NRF_EVT_FLASH_OPERATION_ERROR:	
			DEBUG("flash_sys_event_handler(): NRF_EVT_FLASH_OPERATION_ERROR\r\n");
			flash_state_ = FLASH_OP_ERROR;
			break;
	}
}




uint8_t erase_FLASH(uint32_t page_address, uint8_t is_blocking)
{	
	flash_state_ = FLASH_WAIT_FOR_OP;

	switch(sd_flash_page_erase((page_address & 0x0007FFFF) / 0x1000))
	{
		case NRF_ERROR_INTERNAL:
			return ERASE_FLASH_ERROR_INTERNAL;	 

		case NRF_ERROR_INVALID_ADDR:
			return ERASE_FLASH_ERROR_INVALID_ADDR;	

		case NRF_ERROR_BUSY:
			return ERASE_FLASH_ERROR_BUSY;		

		case NRF_ERROR_FORBIDDEN:
			return ERASE_FLASH_ERROR_FORBIDDEN; 

		case NRF_SUCCESS:
			if(is_blocking)
			{
				while(flash_state_ == FLASH_WAIT_FOR_OP);	

				if(flash_state_ == FLASH_OP_SUCCESS)
					return ERASE_FLASH_SUCCESS;	
				else
					return ERASE_FLASH_ERROR_FLASH_OP;	
			}
			else
				return ERASE_FLASH_SUCCESS;

		default:
			return ERASE_FLASH_UNKNOWN_ERROR;
	}	
}





uint8_t write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t sz, uint8_t is_blocking)
{

	flash_state_ = FLASH_WAIT_FOR_OP;

	switch(sd_flash_write(flash_address, src, sz))
	{
		case NRF_ERROR_INVALID_ADDR:
			return WRITE_FLASH_ERROR_INVALID_ADDR;

		case NRF_ERROR_BUSY:
			return WRITE_FLASH_ERROR_BUSY;

		case NRF_ERROR_INVALID_LENGTH:
			return WRITE_FLASH_ERROR_INVALID_LENGTH;
		
		case NRF_ERROR_FORBIDDEN:
			return WRITE_FLASH_ERROR_FORBIDDEN;

		case NRF_SUCCESS:
			if(is_blocking)
			{
				while(flash_state_ == FLASH_WAIT_FOR_OP);
				/*
				{
					DEBUG("write flash_state_: %d\r\n", flash_state_);
				}
				*/

				if(flash_state_ == FLASH_OP_SUCCESS)
					return WRITE_FLASH_SUCCESS;	
				else
					return WRITE_FLASH_ERROR_FLASH_OP;	
			}
			else
				return WRITE_FLASH_SUCCESS;

		default:
			return WRITE_FLASH_UNKNOWN_ERROR; 
	}
}









/*
void erase_FLASH(uint32_t page_address)
{	
	
	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// READY => '0'b -> NVMC is busy (on-going write or erase operation)
	// 					'1'b -> NVMC is ready
	//while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 
	//DEBUG("erase_FLASH(): NVMC is ready\r\n");


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// WEN	=> '00'b -> Read only access
	// 		  => '01'b -> Write enabled
	// 		  => '10'b -> Erase enabled
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos;
	DEBUG("erase_FLASH(): NVMC is configured in erase mode\r\n");

	while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 
	DEBUG("erase_FLASH(): NVMC is ready 2\r\n");


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// ERASEPAGE => The value is the address of the page to be erased.
	// 							(Address of the first word in the page.)
	NRF_NVMC->ERASEPAGE = page_address & 0xFFFFE000;
	DEBUG("erase_FLASH(): ERASEPAGE is set to page_address\r\n");

	// Wait for ongoing erase operation to be compleated
	while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	DEBUG("erase_FLASH(): NVMC is ready\r\n");

	// Return FLASH to read only access mode
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;

	while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	DEBUG("erase_FLASH(): NVMC is ready\r\n");
}
*/



/*
void write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t src_len)
{
	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// READY => '0'b -> NVMC is busy (on-going write or erase operation)
	// 					'1'b -> NVMC is ready
	while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// WEN	=> '00'b -> Read only access
	// 		  => '01'b -> Write enabled
	// 		  => '10'b -> Erase enabled
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;


	for(uint32_t idx = 0; idx < src_len; ++idx)
	{	
		flash_address[idx] = src[idx];
		
		// Wait for ongoing write operation to be compleated
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	}


	// Return FLASH to read only access mode
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
}
*/
