#include "step_count_db.h"


uint8_t update_step_count(uint32_t step_count_to_add, uint32_t current_timestamp)
{
	int32_t most_recent_stamp_idx = -1;
	int32_t first_empty_slot_idx = -1;	
	uint32_t step_count = 0;

	// Find the last stamp of step count for the current day
	// If that value does not exist, then start count from 0
	// else add 'step_count_to_add' to that value,
	// and after that save it in free slot
	for(int32_t idx = 0; idx < STEP_COUNT_FLASH_SZ; idx += 2)	
	{	
		// Check does slot have meaningful value(that is not empty/reset)	
		if(step_count_flash[idx] 	 != 0xFFFFFFFF && 
			 step_count_flash[idx+1] != 0xFFFFFFFF  )
		{
			// Check is this timestamp for the current day
			if(step_count_flash[idx] / 86400 == current_timestamp / 86400)			
			{
				// Check is current_timestamp the most recent for it's day, if not then return error message	
				if(current_timestamp <= step_count_flash[idx])
					return USC_ERROR_TIMESTAMP_IS_NOT_MOST_RECENT; 
			
				// Check is this stamp more recent then previous, which were found  
				if(most_recent_stamp_idx == -1)
					most_recent_stamp_idx = idx;
				else if(step_count_flash[most_recent_stamp_idx] < step_count_flash[idx])
					most_recent_stamp_idx = idx;
				
			}		
		}
		// Remember first empty slot 
		else if(first_empty_slot_idx == -1             &&
						step_count_flash[idx] 	 == 0xFFFFFFFF && 
			 		  step_count_flash[idx+1]  == 0xFFFFFFFF  )
		{
			first_empty_slot_idx = idx; 
		}
	}


	if(most_recent_stamp_idx != 1)
		step_count = step_count_flash[most_recent_stamp_idx + 1] + step_count_to_add;  
	else
		step_count = step_count_to_add;


	// ||============ SECTOR1 ==+===========||================= SECTOR2 ================||==================== SECTOR3 ===============||
	// || SLOT1 || SLOT2 || ... || SLOT 512 || SLOT 513 || SLOT 514 || ... || SLOT 1024 || SLOT 1025 || SLOT 1026 || ... || SLOT 1536 ||

	// If empty slot is not found then delete sector next to sector where
	// the most recent stamp was found
	DEBUG("update_step_count(): first_empty_slot_idx: %d\r\n", first_empty_slot_idx);

	if(first_empty_slot_idx == -1)
	{
		uint32_t erase_page_idx = most_recent_stamp_idx / 1024 + 1;

		if(erase_page_idx == 3)
			erase_page_idx = 0;

		first_empty_slot_idx = 1024 * erase_page_idx; 

		erase_FLASH((uint32_t)&step_count_flash[first_empty_slot_idx], 1);
	}


	// Write stamp to FLASH
	{
		uint32_t src[2] = { 0x0, 0x0 };
	
		src[0] = current_timestamp;
		src[1] = step_count;

		if(write_FLASH(&step_count_flash[first_empty_slot_idx], src, 2, 1) != WRITE_FLASH_SUCCESS)
			return USC_WRITE_FLASH_ERROR;	
	}

	return USC_SUCCESS;	
}





uint8_t get_most_recent_step_count(uint32_t *step_count, uint32_t *timestamp, uint32_t current_timestamp)
{
	
	int32_t most_recent_stamp_idx = -1;
	
	// Find the most recent timestamp of step count for the current day for 'current_timestamp'
	// If that value does not exist, return error message to user
	for(int32_t idx = 0; idx < STEP_COUNT_FLASH_SZ; idx += 2)	
		// Check does slot have meaningful value(that is not empty)	
		if(step_count_flash[idx] 	 != 0xFFFFFFFF && 
			 step_count_flash[idx+1] != 0xFFFFFFFF  )
		{
			// Check is this timestamp for the current day
			if(step_count_flash[idx] / 86400 == current_timestamp / 86400)			
			{
				// Only stamp with timestamp before 'current_timestamp' will be take into account	
				if(current_timestamp < step_count_flash[idx])
					continue;

				// Check is this stamp more recent then previous, which were found  
				if(most_recent_stamp_idx == -1)
					most_recent_stamp_idx = idx;
				else if(step_count_flash[most_recent_stamp_idx] < step_count_flash[idx])
					most_recent_stamp_idx = idx;
				
			}		
		}


	if(most_recent_stamp_idx == -1)
		return MRV_ERROR_NOT_FOUND;

	*timestamp  = step_count_flash[most_recent_stamp_idx];  
	*step_count = step_count_flash[most_recent_stamp_idx + 1];  

	return MRV_SUCCESS;
}


