#ifndef SPIM_H_
#define SPIM_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include "gpio.h"
#include "print.h"


#define SPIM_FREQUENCY_125000 		SPIM_FREQUENCY_FREQUENCY_K125
#define SPIM_FREQUENCY_250000     SPIM_FREQUENCY_FREQUENCY_K250
#define SPIM_FREQUENCY_500000     SPIM_FREQUENCY_FREQUENCY_K500
#define SPIM_FREQUENCY_1000000    SPIM_FREQUENCY_FREQUENCY_M1
#define SPIM_FREQUENCY_2000000    SPIM_FREQUENCY_FREQUENCY_M2 
#define SPIM_FREQUENCY_4000000    SPIM_FREQUENCY_FREQUENCY_M4 
#define SPIM_FREQUENCY_8000000    SPIM_FREQUENCY_FREQUENCY_M8


// ==================== CONFIG FLAGS ============================
// ==============================================================

#define SPIM_CPOL_ACTIVE_HIGH 	  SPIM_CONFIG_CPOL_ActiveHigh
#define SPIM_CPOL_ACTIVE_LOW			SPIM_CONFIG_CPOL_ActiveLow 

#define SPIM_CPHA_LEADING					SPIM_CONFIG_CPHA_Leading 
#define SPIM_CPHA_TRAILING				SPIM_CONFIG_CPHA_Trailing 

#define SPIM_ORDER_MSBFIRST       SPIM_CONFIG_ORDER_MsbFirst 
#define SPIM_ORDER_LSBFIRST 			SPIM_CONFIG_ORDER_LsbFirst

// ==============================================================
// ==============================================================


void init_SPIM0(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint32_t freq, uint32_t config_flags);
void config_SPIM0_receive_buffer(char *receive_buff, uint32_t receive_buff_len);
void config_SPIM0_transmit_buffer(char *transmit_buff, uint32_t transmit_buff_len);


void init_SPIM1(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint32_t freq, uint32_t config_flags);
void config_SPIM1_receive_buffer(char *receive_buff, uint32_t receive_buff_len);
void config_SPIM1_transmit_buffer(char *transmit_buff, uint32_t transmit_buff_len);


// ===================================== SEND DATA ============================================
// ============================================================================================

#define SEND_DATA_RETURN_VALUE(byte_sent, byte_received) 		 (((byte_sent & 0xFF) << 8) | (byte_received & 0xFF)) 
#define SEND_DATA_RETURN_VALUE_BYTE_SENT(return_value) 	 		 ((return_value >> 8) & 0xFF)
#define SEND_DATA_RETURN_VALUE_BYTE_RECEIVED(return_value) 	 (return_value & 0xFF)

uint32_t send_data_SPIM0(char *tx_buff, uint32_t tx_buff_len, char *rx_buff, uint32_t rx_buff_len);
uint32_t send_data_SPIM1(char *tx_buff, uint32_t tx_buff_len, char *rx_buff, uint32_t rx_buff_len);

// ============================================================================================
// ============================================================================================


#endif
