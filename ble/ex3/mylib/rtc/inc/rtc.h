#ifndef RTC_H_
#define RTC_H_

#include <stddef.h>
#include <nrf52.h>
#include <nrf52_bitfields.h>


void init_RTC2();
void set_RTC2_IRQ_callback(void (*RTC2_IRQ_callback)(void));


#endif
