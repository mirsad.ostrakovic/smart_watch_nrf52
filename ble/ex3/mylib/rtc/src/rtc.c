#include "rtc.h"



// ====================================== RTC 2 ===============================================
// ============================================================================================

static void (*RTC2_IRQ_callback_)(void) = NULL;



void init_RTC2()
{
	// (nRF52832 Product Specification v1.4, RTC, page 249)
	// TICK <=> '1'b => Enable interrupt for TICK event
	NRF_RTC2->INTENSET = RTC_INTENSET_TICK_Enabled;  


	// (nRF52832 Product Specification v1.4, RTC, page 251)
	// TICK <=> '1'b => Enable interrupt for TICK event
	NRF_RTC2->EVTENSET = RTC_EVTENSET_TICK_Enabled; 


	// (nRF52832 Product Specification v1.4, RTC, page 253)
	// f_RTC = 32768Hz / (PRESCALER + 1);	
	NRF_RTC2->PRESCALER = 32768 - 1;


	// Enable interrupt for RTC2
	NVIC_SetPriority(RTC2_IRQn, 32);
	NVIC_EnableIRQ(RTC2_IRQn);


	// (nRF52832 Product Specification v1.4, RTC, page 249)
	// Start RTC
	NRF_RTC2->TASKS_START = 1;
}



void set_RTC2_IRQ_callback(void (*RTC2_IRQ_callback)(void))
{
	RTC2_IRQ_callback_ = RTC2_IRQ_callback;  
}



void RTC2_IRQHandler(void)
{
	if(RTC2_IRQ_callback_ != NULL)
		RTC2_IRQ_callback_(); 

	// Cleare TICK event flag, if it is not done, than app will lock in RTC2_IRQHandler()
	NRF_RTC2->EVENTS_TICK = 0;
}


// ============================================================================================
// ============================================================================================
