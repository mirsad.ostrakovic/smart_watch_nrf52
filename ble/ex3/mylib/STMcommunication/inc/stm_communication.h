#ifndef STM_COMMUNICATION_H_
#define STM_COMMUNICATION_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include "spim.h"
#include "debug.h"

// ============= STM COMMUNICATION PIN CONFIGURATION ==============
// ================================================================

#define STM_COMMUNICATION_SCK_PIN 	13
#define STM_COMMUNICATION_MOSI_PIN  14 
#define STM_COMMUNICATION_MISO_PIN  15
#define STM_COMMUNICATION_CS_PIN    16


#define WRITE_REGISTER_COMMAND 0xA
#define READ_REGISTER_COMMAND  0XB
#define WRITE_IMAGE_COMMAND		 0xC


#define REGISTER_RTC_YEAR	 		0x10
#define REGISTER_RTC_MONTH  	0x11
#define REGISTER_RTC_DAY			0x12
#define REGISTER_RTC_WDU			0x13
#define REGISTER_RTC_HOUR			0x14
#define REGISTER_RTC_MINUTE 	0x15
#define REGISTER_RTC_SECOND 	0x16
#define REGISTER_RTC_COMMAND  0x17





void init_STMcommunication(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint8_t cs_pin);

uint8_t read_register_STMcommunication(uint8_t reg_addr);
void write_register_STMcommunication(uint8_t reg_addr, uint8_t data);
void write_image_packet_STMcommunication(uint8_t packet_sz, uint16_t data_pos, uint8_t *data);

#endif
