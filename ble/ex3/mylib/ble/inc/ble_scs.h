#ifndef BLE_SCS_H_
#define BLE_SCS_H_


#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "sdk_common.h"
#include "boards.h"
#include "debug.h"


// ========================================================================================
// ======================= SCS(Step count custom service) =================================
// ========================================================================================

#define BLE_SCS_UUID_BASE { 0x1A, 0xA0, 0xFC, 0x1C, 0xC4, 0x62, 0x76, 0x98, \
													  0x61, 0x47, 0x95, 0xCB, 0xCE, 0xB6, 0xBE, 0xC1 }

#define BLE_SCS_SERVICE_UUID 			0x1700
#define BLE_SCS_VALUE_CHAR_UUID 	0x1701

typedef enum
{
  	BLE_SCS_EVT_NOTIFICATION_ENABLED,                 
  	BLE_SCS_EVT_NOTIFICATION_DISABLED 
} ble_scs_evt_type_t;


typedef struct{
  	ble_scs_evt_type_t evt_type;
} ble_scs_evt_t;


// --------------------------------------------------------------------------------------------
// forward declaration of the ble_scs_t, needed for 'ble_scs_evt_handler_t' declaration
// --------------------------------------------------------------------------------------------
typedef struct ble_scs_s ble_scs_t;


// --------------------------------------------------------------------------------------------
// ble_scs_evt_handler_t 		==> Step count custom service event handler type 	
// --------------------------------------------------------------------------------------------
typedef void (*ble_scs_evt_handler_t) (ble_scs_t * p_scs, ble_scs_evt_t * p_evt);


// --------------------------------------------------------------------------------------------
// ble_scs_evt_handler_t		==> event handler to be called for handling events in the SCS 
// --------------------------------------------------------------------------------------------
// support_notification 		==> if TRUE then notification for the step count is enabled
// --------------------------------------------------------------------------------------------
// initial_step_count				==> initial read for step count
// --------------------------------------------------------------------------------------------
// step_count_char_attr_md 	==> initial security level for step count characteristics attribute
// --------------------------------------------------------------------------------------------
typedef struct
{
    ble_scs_evt_handler_t         evt_handler;                    
    bool                          support_notification;      
    uint32_t                      initial_step_count;   
    ble_srv_cccd_security_mode_t  step_count_char_attr_md;
} ble_scs_init_t;



// --------------------------------------------------------------------------------------------------
// evt_handler: 				 		 ==>	event handler to be called for handling events in the SCS
// --------------------------------------------------------------------------------------------------
// service_handle						 ==>  handle of step count custom service (as provided by the BLE stack) 
// --------------------------------------------------------------------------------------------------
// step_count_handles		 		 ==>  handles related to the step count characteristic 
// --------------------------------------------------------------------------------------------------
// step_count_last			 		 ==>  last step count passed to the step count service
// --------------------------------------------------------------------------------------------------
// conn_handle 							 ==> 	handle of the currect connection (as provided by the BLE stack,
// 																has value BLE_CONN_HANDLE_INVALID if is is not in a connection)
// --------------------------------------------------------------------------------------------------
// is_notification_supported ==> true if notification of battery level is supported
// --------------------------------------------------------------------------------------------------
struct ble_scs_s
{ 
		uint8_t												uuid_type;
		ble_scs_evt_handler_t         evt_handler;
    uint16_t                      service_handle;
    ble_gatts_char_handles_t      step_count_handles;
    uint32_t                      step_count_last;
    uint16_t                      conn_handle;
    bool                          is_notification_supported;
};



// function for initializing the Step count custom service
uint32_t ble_scs_init(ble_scs_t *p_scs, const ble_scs_init_t *p_scs_init);

// function for handling the Step count service's BLE events
void ble_scs_on_ble_evt(ble_scs_t *p_scs, ble_evt_t *p_ble_evt);

// function which handles step count update in SCS service
uint32_t ble_scs_step_count_update(ble_scs_t * p_scs, uint32_t step_count);

#endif
