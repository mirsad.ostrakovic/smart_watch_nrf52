#include "ble_init.h"


// LED custom service state variable
ble_lcs_t ble_lcs;

// image custom service state variable
ble_ics_t ble_ics;

// time custom service state variable
ble_tcs_t ble_tcs;

// step count custom service state variable
ble_scs_t ble_scs;


static void sys_evt_dispatch(uint32_t sys_evt);
static void ble_evt_dispatch(ble_evt_t * p_ble_evt);
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
static void on_ble_evt(ble_evt_t * p_ble_evt);
static void pm_evt_handler(pm_evt_t const * p_evt);

static void advertising_start();

// Handle of the current connection
uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                 


// GATT module instance
// ----------------------------------------------------------------
// 'nrf_ble_gatt_t' in '/components/ble/nrf_ble_gatt/nrf_ble_gatt.h'
// is defined as typedef for 'nrf_ble_gatt_s'
// 'nrf_ble_gatt_s' is also defined in 'nrf_ble_gatt.h'
// ----------------------------------------------------------------
nrf_ble_gatt_t m_gatt;                                                   



// Use UUIDs for services used application
// ------------------------------------------------------------------
// BLE UUID types are defined in:
// Defined in '/components/softdevice/s132/headers/ble_types.h'		
// ------------------------------------------------------------------	
ble_uuid_t m_adv_uuids[] = {};//{{USER_LED_SERVICE_UUID, BLE_UUID_TYPE_VENDOR_BEGIN}}; 












// ================= APP_ERROR_CHECK() ==================
// Defined in '/components/libraries/util/app_error.h'
//            '/components/libraries/util/app_error.c'
// ======================================================


static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    //APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    //err_code = bsp_btn_ble_sleep_mode_prepare();
    //APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}






// Function for dispatching a system event to interested modules
// This function is called from the System event interrupt 
// handler after a system event has been received
static void sys_evt_dispatch(uint32_t sys_evt)
{
    // Dispatch the system event to the fstorage module, where it will be
    // dispatched to the Flash Data Storage (FDS) module.
    fs_sys_event_handler(sys_evt);

    // Dispatch to the Advertising module last, since it will check if there are any
    // pending flash operations in fstorage. Let fstorage process system events first,
    // so that it can report correctly to the Advertising module.
    ble_advertising_on_sys_evt(sys_evt);


		//FLASH system event happened
		flash_sys_event_handler(sys_evt);
}






// Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
// This function is called from the BLE Stack event interrupt handler after a BLE stack
// event has been received.
static void ble_evt_dispatch(ble_evt_t *p_ble_evt)
{
 		// Function to provide BLE event to connection state module and it 
		// has to be call at the start of event handler dispatcher 
		ble_conn_state_on_ble_evt(p_ble_evt);
  
		// Function for passing BLE events to the peer manager, it must be
		// called after ble_conn_state_on_ble_evt, but before the application
		// processes the event
		pm_on_ble_evt(p_ble_evt);
	
		// Function for handling apllication connection params
    ble_conn_params_on_ble_evt(p_ble_evt);
    
		//bsp_btn_ble_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
   
		// This function must be called from the BLE stack event dispatcher
		// for the module to handle BLE events that are relevant to the
		// advertising module
		ble_advertising_on_ble_evt(p_ble_evt);
 
		// This function handles all events from the BLE stack that are of 
		// interest to the GATT module
		nrf_ble_gatt_on_ble_evt(&m_gatt, p_ble_evt);


		// Place to add custom service:	
		ble_lcs_on_ble_evt(&ble_lcs, p_ble_evt);	
		ble_ics_on_ble_evt(&ble_ics, p_ble_evt);
	  ble_tcs_on_ble_evt(&ble_tcs, p_ble_evt);	
	  ble_scs_on_ble_evt(&ble_scs, p_ble_evt);
		//ble_LED_service_on_ble_event(p_ble_evt, &LED_service);
}




// Function for handling advertising events
// This function will be called for advertising events which are passed to the application
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    //ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            DEBUG("on_adv_evt(): FAST ADVERTISING MODE\r\n");
            //err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            //APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE: 
            DEBUG("on_adv_evt(): IDLE MODE\r\n");
						sleep_mode_enter();
            break;

        default:
            break;
    }
}







//Function for handling the Application's BLE Stack events.
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
        { 
						DEBUG("on_ble_evt(): DISCONNECTED\r\n");
            //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
				}
				break;



        case BLE_GAP_EVT_CONNECTED:
				{ 
						DEBUG("on_ble_evt(): CONNECTED\r\n");
            //err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        } 
				break; 



        case BLE_GATTC_EVT_TIMEOUT:
       	{   
						// Disconnect on GATT Client timeout event.
            DEBUG("on_ble_evt(): GATT CLIENT TIMEOUT\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
          	APP_ERROR_CHECK(err_code);
        } 
				break;




        case BLE_GATTS_EVT_TIMEOUT:
       	{    
				// Disconnect on GATT Server timeout event.
            DEBUG("on_ble_evt(): GATT SERVER TIMEOUT\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
				} 
				break; 




        case BLE_EVT_USER_MEM_REQUEST: 
        {   
						DEBUG("on_ble_evt(): USER MEMORY REQUEST\r\n");
						err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
        } 
				break;




        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        { 
            DEBUG("on_ble_evt(): RW AUTHORIZE REQUEST\r\n");
						
						ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } 
				break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

        
				default:
            // No implementation needed.
            break;
    }
}






// Function for handling Peer Manager events.
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    ret_code_t err_code;

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            DEBUG("pm_evt_handler(): connected to a previously bonded device\r\n");
        } 
				break;



        case PM_EVT_CONN_SEC_SUCCEEDED:
        {
            DEBUG("pm_evt_handler():" "connection secured: role: %d," 
																		 	"conn_handle: 0x%x," 
																		  "procedure: %d\r\n",
                         ble_conn_state_role(p_evt->conn_handle),
                         p_evt->conn_handle,
                         p_evt->params.conn_sec_succeeded.procedure);
        } 
				break;



        case PM_EVT_CONN_SEC_FAILED:
        {
            DEBUG("pm_evt_handler(): connection securing fail\r\n");
						// Often, when securing fails, it shouldn't be restarted, for security reasons.
            // Other times, it can be restarted directly.
            // Sometimes it can be restarted, but only after changing some Security Parameters.
            // Sometimes, it cannot be restarted until the link is disconnected and reconnected.
            // Sometimes it is impossible, to secure the link, or the peer device does not support it.
            // How to handle this error is highly application dependent. 
        } 
				break;



        case PM_EVT_CONN_SEC_CONFIG_REQ:
        { 
            DEBUG("pm_evt_handler(): secure connection config request\r\n");
						// Reject pairing request from an already bonded peer.
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } 
				break;



        case PM_EVT_STORAGE_FULL:
        {
            DEBUG("pm_evt_handler(): storage full event\r\n");
						// Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                APP_ERROR_CHECK(err_code);
            }
        } 
				break;



        case PM_EVT_PEERS_DELETE_SUCCEEDED:
        { 
            DEBUG("pm_evt_handler(): peers delete succeeded event\r\n");
						advertising_start(false);
        } 
				break;



        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED:
        { 
            DEBUG("pm_evt_handler(): local DB cache apply failed event\r\n");
						// The local database has likely changed, send service changed indications.
            pm_local_database_has_changed();
        } break;




        case PM_EVT_PEER_DATA_UPDATE_FAILED:
        { 
            DEBUG("pm_evt_handler(): peer data update failed event\r\n");
            APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
        } 
				break;



        case PM_EVT_PEER_DELETE_FAILED:
        {
            DEBUG("pm_evt_handler(): peer delete failed event\r\n");
            APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
        } 
				break;




        case PM_EVT_PEERS_DELETE_FAILED:
        {
            DEBUG("pm_evt_handler(): peers delete failed event\r\n");
            APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
        }
				break;



        case PM_EVT_ERROR_UNEXPECTED:
				{ 
            DEBUG("pm_evt_handler(): error unexpected event\r\n");
            APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
        } 
				break;

        case PM_EVT_CONN_SEC_START:
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
        case PM_EVT_PEER_DELETE_SUCCEEDED:
        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
        case PM_EVT_SERVICE_CHANGED_IND_SENT:
        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
        default:
            break;
    }
}









static void ble_app_timer_init(void)
{
		ret_code_t err_code = app_timer_init(); 
    DEBUG("ble_app_timer_init(): app_timer_init()\r\n");
		APP_ERROR_CHECK(err_code);

}









// Function for initialize the BLE stack(the Softdevice and the BLE event interrupt)
static void ble_stack_init(void)
{
    ret_code_t err_code;

		// ----------------------------------------------------------------
		// Defined in '/components/softdevice/s132/headers/nrf_sdm.h'	
		// ----------------------------------------------------------------
		nrf_clock_lf_cfg_t clock_lf_cfg;  

		// --------------------------------------------------------------------------
		// BLE app should always use external crystal or internal RC oscillator
		// as clock source, instead of the synthesized 32.768hKZ clock 
		// because it consumes a way/much more power.	
		// --------------------------------------------------------------------------
		clock_lf_cfg.source = NRF_CLOCK_LF_SRC_XTAL;
		clock_lf_cfg.rc_ctiv = 0;
		clock_lf_cfg.rc_temp_ctiv = 0;
		clock_lf_cfg.xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM;


    // Initialize the SoftDevice handler module.
		// -------------------------------------------------------------------
   	// It will handle dimensioning and allocation of the memory buffer
		// required for reading events from stack, making sure the buffer is
		// correctly aligned. It wil also connect the stack event handler to 
		// the scheduler/RTOS (if specified).	
		// -------------------------------------------------------------------
		SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);


    // Fetch the start address of the application RAM.
		// -------------------------------------------------------------------------------------
		// Defined in '/components/softdevice/common/softdevice_handler/softdevice_handler.c'	
		// -------------------------------------------------------------------------------------
		uint32_t ram_start = 0;
    err_code = softdevice_app_ram_start_get(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Overwrite some of the default configurations for the BLE stack. 
		// -------------------------------------------------------------------------------------
		// Defined in '/components/softdevice/s132/headers/ble.h'	
		// ble_cfg_t is C UNION of types:	ble_conn_cfg_t   
  	//																ble_common_cfg_t 
  	//																ble_gap_cfg_t    
  	//																ble_gatts_cfg_t  
		// -------------------------------------------------------------------------------------
		ble_cfg_t ble_cfg;


		// -------------------------------------------------------------------------------------
		// Defined in '/components/softdevice/s132/headers/ble.h'	
		// Set number of services used by SoftDevice to 0, if we need to add
		// new services then this value need to be increased 
		// -------------------------------------------------------------------------------------

		memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.common_cfg.vs_uuid_cfg.vs_uuid_count = 5;
    err_code = sd_ble_cfg_set(BLE_COMMON_CFG_VS_UUID, &ble_cfg, ram_start);
		DEBUG("ble_stack_init(): sd_ble_cfg_set(BLE_COMMON_CFG_VS_UID) return %d\r\n", err_code); 	
		APP_ERROR_CHECK(err_code);

		memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.gatts_cfg.attr_tab_size.attr_tab_size = 2000;
    err_code = sd_ble_cfg_set(BLE_GATTS_CFG_ATTR_TAB_SIZE, &ble_cfg, ram_start);
		DEBUG("ble_stack_init(): sd_ble_cfg_set(BLE_GATTS_CFG_ATTR_TAB_SIZE) return %d\r\n", err_code); 	
		APP_ERROR_CHECK(err_code);


    // Configure the maximum number of connections. 
		// -------------------------------------------------------------------------------------
		// Defined in '/components/softdevice/s132/headers/ble_gap.h'		
		// -------------------------------------------------------------------------------------
		memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.gap_cfg.role_count_cfg.periph_role_count  = BLE_GAP_ROLE_COUNT_PERIPH_DEFAULT; // 1
    ble_cfg.gap_cfg.role_count_cfg.central_role_count = 0;
    ble_cfg.gap_cfg.role_count_cfg.central_sec_count  = 0;
    err_code = sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT, &ble_cfg, ram_start); 
		DEBUG("ble_stack_init(): sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT) return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = softdevice_enable(&ram_start); 
		DEBUG("ble_stack_init(): softdevice_enable() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch); 	
		DEBUG("ble_stack_init(): softdevice_ble_evt_handler_set(ble_evt_dispatch) return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch); 	
		DEBUG("ble_stack_init(): softdevice_sys_evt_handler_set(sys_evt_dispatch) return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}






// ====================================================================== 
// ========================== SVCALL() ==================================
// Defined in '/components/softdevice/s132/headers/nrf_svc.h'		
// ====================================================================== 







// Function for GAP(generic access profile) initialization,
// it sets up all the necessar GAP params of the device:
// 		-> device name
// 		-> appearance
// 		-> preffered connection params
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

		// ------------------------------------------------------------------
		// Defined in '/components/softdevice/s132/headers/ble_gap.h'		
		// Set connection to require no protection (aka. open link)	
		// ------------------------------------------------------------------
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

		// -----------------------------------------------------------------
		// Call SoftDevice SVC to set device name
		// -----------------------------------------------------------------
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    
		DEBUG("gap_params_init(): sd_ble_gap_device_name_set() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);


		// ------------------------------------------------------------------
		// BLE appearances are defined in:
		// Defined in '/components/softdevice/s132/headers/ble_types.h'		
		// ------------------------------------------------------------------	
		err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_WATCH);  	
		DEBUG("gap_params_init(): sd_ble_gap_appearance_set() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code); 


		// ------------------------------------------------------------------	
		// Set GAP peripheral preferred connection params
		// Defined in '/components/softdevice/s132/headers/ble_gap.h'		 	
		// ------------------------------------------------------------------
		memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params); 
		DEBUG("gap_params_init(): sd_ble_gap_ppcp_set() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}








// Function for initializing the GATT module.
static void gatt_init(void)
{
		// ---------------------------------------------------------------
		// Defined in '/components/ble/nrf_ble_gatt/nrf_ble_gatt.c'	
		// ---------------------------------------------------------------
		ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL); 
		DEBUG("gatt_init(): nrf_ble_gatt_init() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}


// Function for initializing the advertising functionality
static void advertising_init(void)
{
    ret_code_t             	  err_code;
    ble_advdata_t          	  advdata;
    ble_adv_modes_config_t 	  options;
		ble_advdata_manuf_data_t  manuf_data;

    // Build advertising data struct to pass into ble_advertising_init() 
		// ---------------------------------------------------------------
		// Defined in '/components/ble/common/ble_advdata.h'	
		// ---------------------------------------------------------------
		memset(&advdata, 0, sizeof(advdata));

		// Include full device name in advertising data
    advdata.name_type               = BLE_ADVDATA_FULL_NAME;		
		// Include apperance in advertising data
    advdata.include_appearance      = true; 
		// Defined in '/components/softdevice/s132/headers/ble_gap.h'
		// Bluetooth LOW ENERGY general discovery mode
		advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE; 
		advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]); 
		advdata.uuids_complete.p_uuids  = m_adv_uuids;


		// --------------------------------------------------------
		// Setup manufacturer specific data	
		// --------------------------------------------------------
		uint8_t manuf_data_str[] = { 0x15, 0x03, 0x19, 0x97 };
		manuf_data.company_identifier  = 0xFFFF;
		manuf_data.data.p_data = manuf_data_str;
		manuf_data.data.size = sizeof(manuf_data_str);
		advdata.p_manuf_specific_data = &manuf_data;


		// --------------------------------------------------------
		// SoftDevice advertising state:
		// Direct -> Direct Slow -> Fast -> Slow -> Idle
		// If some of a state is disabled, then it will be skipped
		// --------------------------------------------------------
		memset(&options, 0, sizeof(options));
    options.ble_adv_fast_enabled  = true;
    options.ble_adv_fast_interval = APP_ADV_INTERVAL;
    options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

		// ---------------------------------------------------------------
		// Defined in '/components/ble/ble_advertising/ble_advertising.h'	
		// ---------------------------------------------------------------
    err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL); 
		DEBUG("advertising_init(): ble_advertising_init() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}



 // This function will be called for all events in the Connection Parameters Module which
 // are passed to the application. All this function does is to disconnect. 
 // This could have been done by simply setting the disconnect_on_fail config parameter, 
 // but instead we use the event  handler mechanism to demonstrate its use.
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
				DEBUG("on_conn_params_evt(): sd_ble_gap_disconnect() return %d\r\n", err_code); 
				APP_ERROR_CHECK(err_code);
    }
}




// Function for handling a Connection Parameters error.
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}




//Function for initializing the Connection Parameters module.
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

		// ---------------------------------------------------------------
		// Defined in '/components/ble/common/ble_conn_params.h'	
		// ---------------------------------------------------------------
    memset(&cp_init, 0, sizeof(cp_init));

		// If p_conn_params == NULL then the connection params 
		// will be fetched from host
    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
   	// If start_on_notify_cccd_hanldle is set to BLE_GATT_HANGLE_INVALID
		// then procedure is to be started on connection event
		cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
 		DEBUG("conn_params_init(): ble_conn_params_init() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}



// Function for the Peer Manager initialization.
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

		// ---------------------------------------------------------------
		// Defined in '/components/ble/peer_manager/peer_manager.c'	
		// ---------------------------------------------------------------
    err_code = pm_init(); 
 		DEBUG("peer_manager_init(): pm_init() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);  
 		DEBUG("peer_manager_init(): pm_sec_params_set() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
 		DEBUG("peer_manager_init(): pm_register() return %d\r\n", err_code); 
    APP_ERROR_CHECK(err_code);
}




// Function to start advertising
static void advertising_start()
{
    //ret_code_t err_code = pm_peers_delete(); 
 		//DEBUG("advertising_start(): pm_peers_delete() return %d\r\n", err_code); 
		//APP_ERROR_CHECK(err_code);

    ret_code_t err_code = ble_advertising_start(BLE_ADV_MODE_FAST);  
 		DEBUG("advertising_start(): ble_advertising_start() return %d\r\n", err_code); 
		APP_ERROR_CHECK(err_code);
}



// ==============================================================================================
// ==============================================================================================

/*
void ble_lcs_data_handler(ble_lcs_t *p_lcs, uint8_t *data, uint32_t data_len) 
{
	if(p_lcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_lcs_data_handler(): INVALID CALL\r\n");
		return;
	}

	uint8_t last_data = data[data_len - 1];
	data[data_len - 1] = '\0';
	DEBUG("ble_lcs_data_handler(): ->%s%c<-\r\n", data, last_data);
}
*/

// ==============================================================================================
// ==============================================================================================


// Function to ADD custom services
static void services_init(void)
{
	ret_code_t 						err_code;

	// ========================== init LCS(LED custom service) =========================
	// =================================================================================

	ble_lcs_init_t  			lcs_service_init;

	memset(&lcs_service_init, 0, sizeof(lcs_service_init));
	
	lcs_service_init.init_led_state = 0x1;
	lcs_service_init.data_handler = ble_lcs_data_handler;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lcs_service_init.led_state_char_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&lcs_service_init.led_state_char_attr_md.write_perm);
	
	err_code = ble_lcs_init(&ble_lcs, &lcs_service_init);
	APP_ERROR_CHECK(err_code);
	
	DEBUG("service_init(): LCS init compleated\r\n");
	// =================================================================================
	// =================================================================================


	// ======================== init ICS(Image custom service) =========================
	// =================================================================================
	ble_ics_init_t			ics_service_init;

	memset(&ics_service_init, 0, sizeof(ics_service_init));
	
	ics_service_init.data_handler = ble_ics_data_handler;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ics_service_init.tx_char_attr_md.read_perm);	
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ics_service_init.tx_char_attr_md.write_perm);

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ics_service_init.rx_char_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ics_service_init.rx_char_attr_md.write_perm);

	err_code = ble_ics_init(&ble_ics, &ics_service_init);
	APP_ERROR_CHECK(err_code);

	DEBUG("service_init(): ICS init compleated\r\n");
	// =================================================================================
	// =================================================================================


	// ========================= init TCS(Time custom service) =========================
	// =================================================================================
	ble_tcs_init_t			tcs_service_init;

	memset(&tcs_service_init, 0, sizeof(tcs_service_init));
	

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&tcs_service_init.time_char_attr_md.read_perm);	
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&tcs_service_init.time_char_attr_md.write_perm);

	err_code = ble_tcs_init(&ble_tcs, &tcs_service_init);
	APP_ERROR_CHECK(err_code);

	DEBUG("service_init(): TCS init compleated\r\n");
	// =================================================================================
	// =================================================================================



	// =================== init SCS(Step count custom service) =========================
	// =================================================================================
	ble_scs_init_t			scs_service_init;

	memset(&scs_service_init, 0, sizeof(scs_service_init));

	scs_service_init.evt_handler				  = NULL;
  scs_service_init.initial_step_count   = 0;
	scs_service_init.support_notification = 1;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&scs_service_init.step_count_char_attr_md.read_perm);	
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&scs_service_init.step_count_char_attr_md.write_perm);

	err_code = ble_scs_init(&ble_scs, &scs_service_init);
	APP_ERROR_CHECK(err_code);

	DEBUG("service_init(): SCS init compleated\r\n");
	// =================================================================================
	// =================================================================================

}




void init_BLE()
{
    ble_stack_init();
  	DEBUG("init_BLE(): ble_stack_init() => compleated\r\n"); 

		gap_params_init(); 
  	DEBUG("init_BLE(): gap_params_init() => compleated\r\n"); 
		
		gatt_init();
  	DEBUG("init_BLE(): gatt_init() => compleated\r\n"); 
	
		services_init();  
  	DEBUG("init_BLE(): services_init() => compleated\r\n"); 

		advertising_init(); 
  	DEBUG("init_BLE(): advertising_init() => compleated\r\n"); 
		
		ble_app_timer_init();
  	DEBUG("init_BLE(): ble_app_timer_init() => compleated\r\n"); 

		conn_params_init();   
  	DEBUG("init_BLE(): conn_params_init() => compleated\r\n"); 
		
		peer_manager_init();
  	DEBUG("init_ble(): peer_manager_init() => compleated\r\n"); 

		advertising_start();
  	DEBUG("init_ble(): advertising_start() => compleated\r\n"); 
}

