#include "ble_scs.h"


static void on_connect(ble_scs_t * p_scs, ble_evt_t * p_ble_evt)
{
    p_scs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}



static void on_disconnect(ble_scs_t * p_scs, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_scs->conn_handle = BLE_CONN_HANDLE_INVALID;
}



static void on_write(ble_scs_t * p_scs, ble_evt_t * p_ble_evt)
{
	if(p_scs->is_notification_supported)
  {
  	ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

  	if(p_evt_write->handle == p_scs->step_count_handles.cccd_handle &&
       p_evt_write->len == 2)
    {       
			// CCCD written, call application event handler
      if (p_scs->evt_handler != NULL)
      {
      	ble_scs_evt_t evt;

        if(ble_srv_is_notification_enabled(p_evt_write->data))
        	evt.evt_type = BLE_SCS_EVT_NOTIFICATION_ENABLED;
        else
          evt.evt_type = BLE_SCS_EVT_NOTIFICATION_DISABLED;

        p_scs->evt_handler(p_scs, &evt);
      }
    }
  }
}



void ble_scs_on_ble_evt(ble_scs_t * p_scs, ble_evt_t * p_ble_evt)
{
  if(p_scs == NULL || p_ble_evt == NULL)
  	return;

  switch (p_ble_evt->header.evt_id)
  {
  	case BLE_GAP_EVT_CONNECTED:
    	on_connect(p_scs, p_ble_evt);
      break;

    case BLE_GAP_EVT_DISCONNECTED:
      on_disconnect(p_scs, p_ble_evt);
      break;

    case BLE_GATTS_EVT_WRITE:
    	on_write(p_scs, p_ble_evt);
      break;

    default:
      // No implementation needed
      break;
  }
}





static uint32_t step_count_char_add(ble_scs_t * p_scs, const ble_scs_init_t * p_scs_init)
{
		uint32_t            	err_code;
    ble_gatts_char_md_t 	char_md;
    ble_gatts_attr_md_t 	cccd_md;
    ble_gatts_attr_t    	attr_char_value; 
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t          	ble_uuid;
    ble_gatts_attr_md_t 	attr_md;

	
		
		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		// ------------------------------------------------------------------------------------------------
		if(p_scs->is_notification_supported)
		{
			memset(&cccd_md, 0, sizeof(cccd_md));

			BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
			BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

			//cccd_md.read_perm  = step_count_char_attr_md.read_perm; 
			//cccd_md.write_perm = step_count_char_attr_md.write_perm;

			cccd_md.vloc = BLE_GATTS_VLOC_STACK;
		}	
		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		// -------------------------- GATT characteristic presentation format -----------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));

		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT32;
		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------


		// ------------------------------ GATT characteristic metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.notify		 = p_scs->is_notification_supported ? 1 : 0;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = p_scs->is_notification_supported ? &cccd_md : NULL;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "Step count custom service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------


		// ----------------------------------- GATT attribute metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_scs_init->step_count_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_scs_init->step_count_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		ble_uuid.type = p_scs->uuid_type;
		ble_uuid.uuid = BLE_SCS_VALUE_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint32_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint32_t);

		attr_char_value.p_value = (uint8_t*)&(p_scs_init->initial_step_count);
		p_scs->step_count_last  = p_scs_init->initial_step_count;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------




		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		err_code = sd_ble_gatts_characteristic_add(p_scs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_scs->step_count_handles);

		DEBUG("step_count_char_add(): sd_ble_gatts_characteristic_add() return value: %d\r\n", err_code);

		return err_code;
}


uint32_t ble_scs_init(ble_scs_t * p_scs, const ble_scs_init_t * p_scs_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t scs_base_uuid = {BLE_SCS_UUID_BASE};

    VERIFY_PARAM_NOT_NULL(p_scs);
    VERIFY_PARAM_NOT_NULL(p_scs_init);

    // Initialize the service structure
    p_scs->conn_handle               = BLE_CONN_HANDLE_INVALID;
    p_scs->evt_handler               = p_scs_init->evt_handler;	
    p_scs->is_notification_supported = p_scs_init->support_notification;
		//p_scs->is_notification_enabled = false;

    // Add a custom base UUID 
		err_code = sd_ble_uuid_vs_add(&scs_base_uuid, &p_scs->uuid_type);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_scs_init(): sd_ble_uuid_vs_add() success\r\n");

    ble_uuid.type = p_scs->uuid_type;
    ble_uuid.uuid = BLE_SCS_SERVICE_UUID;

    // Add the service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_scs->service_handle); 
		VERIFY_SUCCESS(err_code);
		DEBUG("ble_scs_init(): sd_ble_gatts_service_add() success\r\n");

    // Add the step count characteristic
    err_code = step_count_char_add(p_scs, p_scs_init); 
		VERIFY_SUCCESS(err_code);
		DEBUG("ble_scs_init(): step_count_char_add() success\r\n");

    return NRF_SUCCESS;
}



uint32_t ble_scs_step_count_update(ble_scs_t * p_scs, uint32_t step_count)
{
  if(p_scs == NULL)
  	return NRF_ERROR_NULL;

  uint32_t err_code = NRF_SUCCESS;
  ble_gatts_value_t gatts_value;


  if(step_count != p_scs->step_count_last)
  {
  	// Initialize value struct
    memset(&gatts_value, 0, sizeof(gatts_value));

    gatts_value.len     = sizeof(uint32_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = (uint8_t*)&step_count;

    // Update database
    err_code = sd_ble_gatts_value_set(p_scs->conn_handle,
                                      p_scs->step_count_handles.value_handle,
                                      &gatts_value);
    
		// If GATTS value is update successfully then also update value in '*p_scs' structure 
		if(err_code == NRF_SUCCESS)
    	p_scs->step_count_last = step_count;
    else
    	return err_code;

    // Send value if connected and notifying.
    if ((p_scs->conn_handle != BLE_CONN_HANDLE_INVALID) && p_scs->is_notification_supported)
    {
    	ble_gatts_hvx_params_t hvx_params;

      memset(&hvx_params, 0, sizeof(hvx_params));

      hvx_params.handle = p_scs->step_count_handles.value_handle;
      hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
			hvx_params.offset = gatts_value.offset;
      hvx_params.p_len  = &gatts_value.len;
      hvx_params.p_data = gatts_value.p_value;

      err_code = sd_ble_gatts_hvx(p_scs->conn_handle, &hvx_params);
    }
    else
    	err_code = NRF_ERROR_INVALID_STATE;
  }

  return err_code;
}
