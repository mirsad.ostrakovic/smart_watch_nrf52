#ifndef PRINT_H_
#define PRINT_H_

#include <stdarg.h>
#include "uart.h"


#define init_PRINT() 	init_UART(UART_BAUDRATE_DEFAULT, UART_TX_PIN_DEFAULT, UART_RX_PIN_DEFAULT)
#define putchar(X) 	 	putchar_UART(X)


void print_string(const char *str);
void print_number(long num);
void print_binary(long num);
void print_hex(long num, uint32_t type_size);
void print(const char *str, ...);
#endif
