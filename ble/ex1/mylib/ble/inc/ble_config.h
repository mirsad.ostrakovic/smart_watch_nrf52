#ifndef BLE_CONFIG_H_
#define BLE_CONFIG_H_

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2    // Reply when unsupported features are requested

#define DEVICE_NAME                     "SmartWatch"                  					// Name of device. Will be included in the advertising data 
#define MANUFACTURER_NAME               "OstrakovicInc" 			                  // Manufacturer. Will be passed to Device Information Service
#define APP_ADV_INTERVAL                300                                     // The advertising interval (in units of 0.625 ms, this value corresponds to 187.5 ms) 
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                     // The advertising timeout in units of seconds 

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        // Minimum acceptable connection interval (0.1 seconds)
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        // Maximum acceptable connection interval (0.2 second)
#define SLAVE_LATENCY                   0                                       // Slave latency
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         // Connection supervisory timeout (4 seconds)

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   // Time from initiating event (connect or start of notification) to first time 
																																								// sd_ble_gap_conn_param_update is called (5 seconds)

#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  // Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds)
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       // Number of attempts before giving up the connection parameter negotiation

#define SEC_PARAM_BOND                  1                                       // Perform bonding
#define SEC_PARAM_MITM                  0                                       // Man In The Middle protection not required
#define SEC_PARAM_LESC                  0                                       // LE Secure Connections not enabled
#define SEC_PARAM_KEYPRESS              0                                       // Keypress notifications not enabled
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    // No I/O capabilities
#define SEC_PARAM_OOB                   0                                       // Out Of Band data not available
#define SEC_PARAM_MIN_KEY_SIZE          7                                       // Minimum encryption key size
#define SEC_PARAM_MAX_KEY_SIZE          16                                      // Maximum encryption key size


#endif
