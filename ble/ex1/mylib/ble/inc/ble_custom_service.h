#ifndef BLE_CUSTOM_SERVICE_H_
#define BLE_CUSTOM_SERVICE_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "sdk_common.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "debug.h"

#define USER_LED_SERVICE_UUID_BASE { 0xBC, 0x8A, 0xBF, 0x45, 0xCA, 0x05, 0x50, 0xBA, \
																	   0x40, 0x42, 0xB0, 0x00, 0xC9, 0xAD, 0x64, 0xF3 }

#define USER_LED_SERVICE_UUID 		0x1400
#define USER_LED_VALUE_CHAR_UUID 	0x1401


// initial_custom_value =>  initial custom value
// custom_value_char_attr_md => initial security level for custom characteristics attribute
typedef struct {
	uint8_t 											initial_custom_value;
	ble_srv_cccd_security_mode_t	custom_value_char_attr_md; 
} ble_LED_service_init_t;


// service_handle	=> handle of custom service (as provided by the BLE stack)
// custom_value_handles => handles related to the custom value characteristic
// conn_handle => handle of the currect connection (as provided by the BLE stack,
// 								has value BLE_CONN_HANDLE_INVALID if is is not in a connection)
struct ble_LED_service_s {
	uint16_t									service_handle;
	ble_gatts_char_handles_t 	custom_value_handles;
	uint16_t 									conn_handle;
	uint8_t										uuid_type;
};


typedef struct ble_LED_service_s ble_LED_service_t;


uint32_t ble_LED_service_init(ble_LED_service_t *LED_service_state, 
															ble_LED_service_init_t *LED_service_init_params);


uint32_t custom_value_char_add(ble_LED_service_t *LED_service_state, 
															 const ble_LED_service_init_t *LED_service_init_params);



void ble_LED_service_on_ble_event(const ble_evt_t *p_ble_evt, void *p_context);

uint32_t ble_cus_custom_value_update(ble_LED_service_t *LED_service_state, uint8_t custom_value);

#endif
