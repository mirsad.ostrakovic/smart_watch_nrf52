#include "stm_communication.h"


static uint8_t STMcommunication_cs_pin;


void init_STMcommunication(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint8_t cs_pin)
{
	STMcommunication_cs_pin = cs_pin;

	configure_GPIO(STMcommunication_cs_pin, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);

	set_GPIO_pin(STMcommunication_cs_pin);

	init_SPIM1(sck_pin, mosi_pin, miso_pin, 
						 SPIM_FREQUENCY_1000000, SPIM_CPOL_ACTIVE_HIGH | SPIM_CPHA_LEADING | SPIM_ORDER_MSBFIRST);

}




uint8_t read_register_STMcommunication(uint8_t reg_addr)
{
	uint8_t transmit_buff[3] = { READ_REGISTER_COMMAND, 0x0, 0x0 };
	uint8_t receive_buff[3] = { 0x00 , 0x0, 0x0 };

	transmit_buff[1] = reg_addr;

	clear_GPIO_pin(STMcommunication_cs_pin);	
	send_data_SPIM1((char*)transmit_buff, 3, (char*)receive_buff, 3);
	set_GPIO_pin(STMcommunication_cs_pin);

	return receive_buff[2];
}



void write_register_STMcommunication(uint8_t reg_addr, uint8_t data)
{
	uint8_t transmit_buff[3] = { WRITE_REGISTER_COMMAND, 0x0, 0x0 };
	uint8_t receive_buff[3] = { 0x00 , 0x0, 0x0 };

	transmit_buff[1] = reg_addr;
	transmit_buff[2] = data;

	clear_GPIO_pin(STMcommunication_cs_pin);	
	send_data_SPIM1((char*)transmit_buff, 3, (char*)receive_buff, 3);
	set_GPIO_pin(STMcommunication_cs_pin);
}



