#ifndef FLASH_H_
#define FLASH_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include "debug.h"

#define FLASH_USER_INFO			__attribute__((__section__(".user_info_flash")))
#define FLASH_MEM_ALIGN			__attribute__((aligned(8)))

extern uint32_t user_info_flash[1024];


void erase_FLASH(uint32_t page_address);
void write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t src_len);

#endif
