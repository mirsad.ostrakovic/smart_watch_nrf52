#include "flash.h"

uint32_t FLASH_USER_INFO  FLASH_MEM_ALIGN			user_info_flash[1024];




void erase_FLASH(uint32_t page_address)
{	
	
	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// READY => '0'b -> NVMC is busy (on-going write or erase operation)
	// 					'1'b -> NVMC is ready
	//while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 
	//DEBUG("erase_FLASH(): NVMC is ready\r\n");


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// WEN	=> '00'b -> Read only access
	// 		  => '01'b -> Write enabled
	// 		  => '10'b -> Erase enabled
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos;
	DEBUG("erase_FLASH(): NVMC is configured in erase mode\r\n");

	while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 
	DEBUG("erase_FLASH(): NVMC is ready 2\r\n");


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// ERASEPAGE => The value is the address of the page to be erased.
	// 							(Address of the first word in the page.)
	NRF_NVMC->ERASEPAGE = page_address & 0xFFFFE000;
	DEBUG("erase_FLASH(): ERASEPAGE is set to page_address\r\n");

	// Wait for ongoing erase operation to be compleated
	while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	DEBUG("erase_FLASH(): NVMC is ready\r\n");

	// Return FLASH to read only access mode
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;

	while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	DEBUG("erase_FLASH(): NVMC is ready\r\n");
}



void write_FLASH(uint32_t *flash_address, uint32_t *src, uint32_t src_len)
{
	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// READY => '0'b -> NVMC is busy (on-going write or erase operation)
	// 					'1'b -> NVMC is ready
	while(NRF_NVMC->READY != NVMC_READY_READY_Ready); 


	// (nRF52832 Product Specification v1.4, SPIM, page 31)
	// WEN	=> '00'b -> Read only access
	// 		  => '01'b -> Write enabled
	// 		  => '10'b -> Erase enabled
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;


	for(uint32_t idx = 0; idx < src_len; ++idx)
	{	
		flash_address[idx] = src[idx];
		
		// Wait for ongoing write operation to be compleated
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy); 
	}


	// Return FLASH to read only access mode
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
}
