#ifndef BLE_INIT_H_
#define BLE_INIT_H_

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "fstorage.h"
#include "fds.h"
#include "peer_manager.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"

#include "ble_config.h"
#include "ble_lcs.h"
#include "ble_ics.h"
#include "ble_tcs.h"
#include "debug.h"

void init_BLE();

#endif
