#ifndef BLE_TCS_H_
#define BLE_TCS_H_


#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "sdk_common.h"
#include "boards.h"
#include "debug.h"

#include "stm_communication.h"

// ========================================================================================
// =========================== TCS(Time custom service) ===================================
// ========================================================================================

#define BLE_TCS_UUID_BASE { 0xE1, 0xDD, 0xFB, 0x15, 0xC7, 0x1C, 0xF5, 0xA5, \
													  0x38, 0x49, 0x98, 0xD7, 0x18, 0x70, 0xE9, 0x86 }

#define BLE_TCS_SERVICE_UUID 							0x1000
#define BLE_TCS_YEAR_CHAR_UUID 						0x1001
#define BLE_TCS_MONTH_CHAR_UUID 					0x1002
#define BLE_TCS_DAY_CHAR_UUID 						0x1003
#define BLE_TCS_WDU_CHAR_UUID 						0x1004
#define BLE_TCS_HOUR_CHAR_UUID 						0x1005
#define BLE_TCS_MINUTE_CHAR_UUID 					0x1006
#define BLE_TCS_SECOND_CHAR_UUID 					0x1007
#define BLE_TCS_UPDATE_CHAR_UUID 					0x1008


// --------------------------------------------------------------------------------------------
// forward declaration of the ble_tcs_t, needed for 'ble_tcs_data_handler_t' declaration
// --------------------------------------------------------------------------------------------
typedef struct ble_tcs_s ble_tcs_t;


// ---------------------------------------------------------------------------------------------
// ble_tcs_data_handler_t		==> Time custom service data handler type 	
// ---------------------------------------------------------------------------------------------
typedef void (*ble_tcs_data_handler_t) (ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len);



// ---------------------------------------------------------------------------------------------
// data_handler 						==> data handler
// ---------------------------------------------------------------------------------------------
// time_char_attr_md 	==> initial security level for time state characteristics attributes
// ---------------------------------------------------------------------------------------------
typedef struct {
		ble_tcs_data_handler_t				data_handler; 
		ble_srv_cccd_security_mode_t	time_char_attr_md; 
} ble_tcs_init_t;




// --------------------------------------------------------------------------------------------
// service_handle: 				 ==>	handle of Time custom service (as provided by the BLE stack)
// --------------------------------------------------------------------------------------------
// ***_handles 			 			 ==> 	handles related to the *** characteristic
// --------------------------------------------------------------------------------------------
// conn_handle 						 ==> 	handle of the currect connection (as provided by the BLE stack,
// 														  has value BLE_CONN_HANDLE_INVALID if is is not in a connection)
// --------------------------------------------------------------------------------------------
// is_notification_enabled ==>  variable to indicate if the peer has enabled notification of
// 															LED state changed
// --------------------------------------------------------------------------------------------
// data_handler					   ==>  event handler to be called for handling LED state change 
// --------------------------------------------------------------------------------------------
struct ble_tcs_s {
		uint8_t										uuid_type;
		uint16_t									service_handle;
		ble_gatts_char_handles_t 	year_handles;
		ble_gatts_char_handles_t 	month_handles;
		ble_gatts_char_handles_t 	day_handles;
		ble_gatts_char_handles_t 	wdu_handles;
		ble_gatts_char_handles_t 	hour_handles;
		ble_gatts_char_handles_t 	minute_handles;
		ble_gatts_char_handles_t 	second_handles;
		ble_gatts_char_handles_t 	update_handles;
		uint16_t 									conn_handle;
		bool 											is_notification_enabled;
};



// function for initializing the Time custom service
uint32_t ble_tcs_init(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init);



// function for handling the Time custom service's BLE events
void ble_tcs_on_ble_evt(ble_tcs_t * p_tcs, ble_evt_t * p_ble_evt);

// function for handling the Time custom service's BLE year (update/read) events
void ble_tcs_year_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len); 

// function for handling the Time custom service's BLE month (update/read) events
void ble_tcs_month_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len); 

// function for handling the Time custom service's BLE day (update/read) events
void ble_tcs_day_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len); 

// function for handling the Time custom service's BLE WDU (update/read) events
void ble_tcs_wdu_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len); 

// function for handling the Time custom service's BLE hour (update/read) events
void ble_tcs_hour_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len); 

// function for handling the Time custom service's BLE minute (update/read) events
void ble_tcs_minute_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len);

// function for handling the Time custom service's BLE second (update/read) events
void ble_tcs_second_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len);

// function for handling the Time custom service's BLE update (update/read) events
void ble_tcs_update_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len);


#endif
