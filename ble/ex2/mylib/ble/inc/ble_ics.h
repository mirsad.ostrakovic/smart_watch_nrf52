#ifndef BLE_ICS_H_
#define BLE_ICS_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "sdk_common.h"
#include "boards.h"
#include "debug.h"


// ========================================================================================
// =========================== ICS(Image custom service) ==================================
// ========================================================================================


#define BLE_ICS_UUID_BASE { 0xE4, 0xCC, 0xDB, 0xE2, 0x2A, 0x2A, 0x65, 0xBB, \
													  0xE9, 0x11, 0x1C, 0xD2, 0x58, 0x5C, 0xD5, 0x41 }

#define BLE_ICS_SERVICE_UUID 			0x1500
#define BLE_ICS_RX_CHAR_UUID 			0x1503 
#define BLE_ICS_TX_CHAR_UUID 			0x1505   



#define OPCODE_LENGTH 1
#define HANDLE_LENGTH 2




#if defined(NRF_BLE_GATT_MAX_MTU_SIZE) && (NRF_BLE_GATT_MAX_MTU_SIZE != 0)
    #define BLE_ICS_MAX_DATA_LEN (NRF_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) 
		/* Maximum length of data (in bytes) that can be transmitted to the peer by Image data service */
#else
    #define BLE_ICS_MAX_DATA_LEN (BLE_GATT_MTU_SIZE_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH) 
		/* Maximum length of data (in bytes) that can be transmitted to the peer by Image data service */
    #warning NRF_BLE_GATT_MAX_MTU_SIZE is not defined.
#endif

// Maximum length of the RX Characteristic (in bytes)
#define BLE_ICS_MAX_RX_CHAR_LEN        BLE_ICS_MAX_DATA_LEN        

// Maximum length of the TX Characteristic (in bytes)
#define BLE_ICS_MAX_TX_CHAR_LEN        BLE_ICS_MAX_DATA_LEN 




// Forward declaration of the ble_ics_t type
typedef struct ble_ics_s ble_ics_t;

// Image custom service event handler type
typedef void (*ble_ics_data_handler_t) (ble_ics_t * p_ics, uint8_t * p_data, uint32_t length);




// --------------------------------------------------------------------------------------------
// data_handler 						==> data handler
// --------------------------------------------------------------------------------------------
// tx_char_attr_md 					==> initial security level for TX characteristics attribute
// --------------------------------------------------------------------------------------------
// rx_char_attr_md 					==> initial security level for RX characteristics attribute
// --------------------------------------------------------------------------------------------
typedef struct
{
    ble_ics_data_handler_t data_handler;
		ble_srv_cccd_security_mode_t	tx_char_attr_md; 
		ble_srv_cccd_security_mode_t	rx_char_attr_md; 
} ble_ics_init_t;


// --------------------------------------------------------------------------------------------
// service_handle: 				 ==>	handle of LED custom service (as provided by the BLE stack)
// --------------------------------------------------------------------------------------------
// tx_handles 			 			 ==> 	handles related to the TX characteristic
// --------------------------------------------------------------------------------------------
// rx_handles 			 			 ==> 	handles related to the RX characteristic
// --------------------------------------------------------------------------------------------
// conn_handle 						 ==> 	handle of the currect connection (as provided by the BLE stack,
// 														  has value BLE_CONN_HANDLE_INVALID if is is not in a connection)
// --------------------------------------------------------------------------------------------
// is_notification_enabled ==>  variable to indicate if the peer has enabled notification of
// 															RX state changed
// --------------------------------------------------------------------------------------------
// data_handler					   ==>  event handler to be called for handling LED state change 
// --------------------------------------------------------------------------------------------
struct ble_ics_s
{
    uint8_t                  uuid_type;               
    uint16_t                 service_handle;      
    ble_gatts_char_handles_t tx_handles;        
    ble_gatts_char_handles_t rx_handles;              
    uint16_t                 conn_handle;    
    bool                     is_notification_enabled; 
    ble_ics_data_handler_t   data_handler;       
};



uint32_t ble_ics_init(ble_ics_t * p_ics, const ble_ics_init_t * p_ics_init);

void ble_ics_on_ble_evt(ble_ics_t * p_ics, ble_evt_t * p_ble_evt);

uint32_t ble_ics_string_send(ble_ics_t * p_ics, uint8_t * p_string, uint16_t length);

void ble_ics_data_handler(ble_ics_t *p_ics, uint8_t *data, uint32_t data_len); 



#endif
