#ifndef BLE_LCS_H_
#define BLE_LCS_H_


#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "sdk_common.h"
#include "boards.h"
#include "debug.h"


// ========================================================================================
// =========================== LCS(LED custom service) ====================================
// ========================================================================================

#define BLE_LCS_UUID_BASE { 0xBC, 0x8A, 0xBF, 0x45, 0xCA, 0x05, 0x50, 0xBA, \
													  0x40, 0x42, 0xB0, 0x00, 0xC9, 0xAD, 0x64, 0xF3 }

#define BLE_LCS_SERVICE_UUID 			0x1400
#define BLE_LCS_VALUE_CHAR_UUID 	0x1401



// --------------------------------------------------------------------------------------------
// forward declaration of the ble_lcs_t, needed for 'ble_lcs_data_handler_t' declaration
// --------------------------------------------------------------------------------------------
typedef struct ble_lcs_s ble_lcs_t;


// --------------------------------------------------------------------------------------------
// ble_lcs_data_handler_t		==> LED custom service data handler type 	
// --------------------------------------------------------------------------------------------
typedef void (*ble_lcs_data_handler_t) (ble_lcs_t *p_lcs, uint8_t *data, uint32_t data_len);



// --------------------------------------------------------------------------------------------
// init_led_state 					==> initial LED state
// --------------------------------------------------------------------------------------------
// data_handler 						==> data handler
// --------------------------------------------------------------------------------------------
// led_state_char_attr_md 	==> initial security level for LED state characteristics attribute
// --------------------------------------------------------------------------------------------
typedef struct {
		bool 													init_led_state;
		ble_lcs_data_handler_t				data_handler; 
		ble_srv_cccd_security_mode_t	led_state_char_attr_md; 
} ble_lcs_init_t;




// --------------------------------------------------------------------------------------------
// service_handle: 				 ==>	handle of LED custom service (as provided by the BLE stack)
// --------------------------------------------------------------------------------------------
// led_state_handles 			 ==> 	handles related to the LED state characteristic
// --------------------------------------------------------------------------------------------
// conn_handle 						 ==> 	handle of the currect connection (as provided by the BLE stack,
// 														  has value BLE_CONN_HANDLE_INVALID if is is not in a connection)
// --------------------------------------------------------------------------------------------
// is_notification_enabled ==>  variable to indicate if the peer has enabled notification of
// 															LED state changed
// --------------------------------------------------------------------------------------------
// data_handler					   ==>  event handler to be called for handling LED state change 
// --------------------------------------------------------------------------------------------
struct ble_lcs_s {
		uint8_t										uuid_type;
		uint16_t									service_handle;
		ble_gatts_char_handles_t 	led_state_handles;
		uint16_t 									conn_handle;
		bool 											is_notification_enabled;
		ble_lcs_data_handler_t		data_handler;
};



// function for initializing the LED custom service
uint32_t ble_lcs_init(ble_lcs_t *p_lcs, const ble_lcs_init_t *p_lcs_init);

// function for handling the LED custom service's BLE events
void ble_lcs_on_ble_evt(ble_lcs_t *p_lcs, ble_evt_t *p_ble_evt);

// function for handling the LED custom service's BLE 'data' events
void ble_lcs_data_handler(ble_lcs_t *p_lcs, uint8_t *data, uint32_t data_len); 


#endif
