#include "ble_custom_service.h"



uint32_t ble_LED_service_init(ble_LED_service_t *LED_service_state, 
															ble_LED_service_init_t *LED_service_init_params)
{
	if(LED_service_state == NULL || LED_service_init_params == NULL)
		return NRF_ERROR_NULL;

	uint32_t err_code;
	ble_uuid_t ble_uuid;


	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&LED_service_init_params->custom_value_char_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&LED_service_init_params->custom_value_char_attr_md.write_perm);


	// Set connection handler state to INVALID (for now)
	LED_service_state->conn_handle = BLE_CONN_HANDLE_INVALID;

	// Get 128-bit UUID type 
	ble_uuid128_t base_uuid = { USER_LED_SERVICE_UUID_BASE };
	err_code = sd_ble_uuid_vs_add(&base_uuid, &LED_service_state->uuid_type);
	VERIFY_SUCCESS(err_code);

	// Set 128-bit UUID of service  
	ble_uuid.type = LED_service_state->uuid_type;
	ble_uuid.uuid = USER_LED_SERVICE_UUID;

	// Init service   
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, 
																			&ble_uuid, 
																			&LED_service_state->service_handle);

	if(err_code != NRF_SUCCESS)
		return err_code;


	return	custom_value_char_add(LED_service_state, LED_service_init_params);
}



uint32_t custom_value_char_add(ble_LED_service_t *LED_service_state, 
															 const ble_LED_service_init_t *LED_service_init_params)
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		//ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// ------------------------------ GATT characteristic metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		char_md.char_props.notify		 = 0;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = NULL;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = NULL;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------



		// ----------------------------------- GATT attribute metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= LED_service_init_params->custom_value_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= LED_service_init_params->custom_value_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------


		ble_uuid.type = LED_service_state->uuid_type;
		ble_uuid.uuid = USER_LED_VALUE_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		err_code = sd_ble_gatts_characteristic_add(LED_service_state->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &LED_service_state->custom_value_handles);


		return err_code;
}





void ble_LED_service_on_ble_event(const ble_evt_t *p_ble_evt, void *p_context)
{

		ble_LED_service_t *LED_service_state = (ble_LED_service_t*)p_context;

		if(LED_service_state == NULL || p_ble_evt == NULL)
				return;

		switch(p_ble_evt->header.evt_id)
		{
				case BLE_GAP_EVT_CONNECTED:
					LED_service_state->conn_handle = p_ble_evt->evt.gap_evt.conn_handle; 
					break;


				case BLE_GATTS_EVT_WRITE:
				{
					const ble_gatts_evt_write_t *p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

					if(p_evt_write->handle == LED_service_state->custom_value_handles.value_handle)
					{
						
						//ble_cus_custom_value_update(LED_service_state, p_evt_write->data[0]);
						DEBUG("TOGGLE LED PIN VALUE\r\n");
					}
				}	
				break;

				case BLE_GAP_EVT_DISCONNECTED:
					LED_service_state->conn_handle = BLE_CONN_HANDLE_INVALID;
					break;

				default:
					break;
		}
	
	

}




uint32_t ble_cus_custom_value_update(ble_LED_service_t *LED_service_state, uint8_t custom_value)
{

		if(LED_service_state == NULL)
				return NRF_ERROR_NULL;


		uint32_t err_code = NRF_SUCCESS;
		ble_gatts_value_t gatts_value;

		DEBUG("New value: %d\r\n", custom_value);

		memset(&gatts_value, 0, sizeof(gatts_value));
		gatts_value.len = sizeof(uint8_t);
		gatts_value.offset = 0;
		gatts_value.p_value = &custom_value;

		return err_code = sd_ble_gatts_value_set(LED_service_state->conn_handle,
																						 LED_service_state->custom_value_handles.value_handle,
																						 &gatts_value);
}
