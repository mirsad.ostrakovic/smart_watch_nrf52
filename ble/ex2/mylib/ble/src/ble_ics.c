#include "ble_ics.h"


static void on_connect(ble_ics_t * p_ics, ble_evt_t * p_ble_evt)
{
    p_ics->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}



static void on_disconnect(ble_ics_t * p_ics, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_ics->conn_handle = BLE_CONN_HANDLE_INVALID;
}



static void on_write(ble_ics_t * p_ics, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if (p_evt_write->handle == p_ics->tx_handles.cccd_handle &&
        p_evt_write->len == 2)
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
            p_ics->is_notification_enabled = true;
        else
            p_ics->is_notification_enabled = false;
    }
    else if (p_evt_write->handle == p_ics->rx_handles.value_handle	&&
             p_ics->data_handler != NULL)
    {
        p_ics->data_handler(p_ics, p_evt_write->data, p_evt_write->len);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}








void ble_ics_on_ble_evt(ble_ics_t * p_ics, ble_evt_t * p_ble_evt)
{
    if (p_ics == NULL || p_ble_evt == NULL)
        return;


    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_ics, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_ics, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_ics, p_ble_evt);
            break;

        default:
            // No implementation needed
            break;
    }
}









static uint32_t tx_char_add(ble_ics_t * p_ics, const ble_ics_init_t * p_ics_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;


    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;




    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_ics->uuid_type;
    ble_uuid.uuid = BLE_ICS_TX_CHAR_UUID;




    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    
		
		memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_ICS_MAX_TX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ics->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ics->tx_handles);
}








static uint32_t rx_char_add(ble_ics_t * p_ics, const ble_ics_init_t * p_ics_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;




    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.write         = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = NULL;
    char_md.p_sccd_md                = NULL;

    ble_uuid.type = p_ics->uuid_type;
    ble_uuid.uuid = BLE_ICS_RX_CHAR_UUID;



    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;



    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_ICS_MAX_RX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ics->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ics->rx_handles);
}









uint32_t ble_ics_init(ble_ics_t * p_ics, const ble_ics_init_t * p_ics_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t ics_base_uuid = {BLE_ICS_UUID_BASE};

    VERIFY_PARAM_NOT_NULL(p_ics);
    VERIFY_PARAM_NOT_NULL(p_ics_init);
		DEBUG("ble_ics_init(): args not null\r\n");

    // Initialize the service structure
    p_ics->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_ics->data_handler            = p_ics_init->data_handler;
    p_ics->is_notification_enabled = false;

    // Add a custom base UUID
    err_code = sd_ble_uuid_vs_add(&ics_base_uuid, &p_ics->uuid_type);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_ics_init(): sd_ble_uuid_vs_add succesffully finished\r\n");

    ble_uuid.type = p_ics->uuid_type;
    ble_uuid.uuid = BLE_ICS_SERVICE_UUID;

    // Add the service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_ics->service_handle);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_ics_init(): BLE Image service created\r\n");

    // Add the RX characteristic
    err_code = rx_char_add(p_ics, p_ics_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_ics_init(): BLE Image service RX char added\r\n");

    // Add the TX characteristic
    err_code = tx_char_add(p_ics, p_ics_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_ics_init(): BLE Image service TX char added\r\n");

    return NRF_SUCCESS;
}





uint32_t ble_ics_string_send(ble_ics_t * p_ics, uint8_t * p_string, uint16_t length)
{
    ble_gatts_hvx_params_t hvx_params;

    VERIFY_PARAM_NOT_NULL(p_ics);

    if ((p_ics->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_ics->is_notification_enabled))
        return NRF_ERROR_INVALID_STATE;

    if (length > BLE_ICS_MAX_DATA_LEN)
        return NRF_ERROR_INVALID_PARAM;


    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_ics->tx_handles.value_handle;
    hvx_params.p_data = p_string;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    return sd_ble_gatts_hvx(p_ics->conn_handle, &hvx_params);
}




void ble_ics_data_handler(ble_ics_t *p_ics, uint8_t *data, uint32_t data_len) 
{
	
	if(p_ics == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_ics_data_handler(): invalid params\r\n");
		return;
	}

	// ==============> FOR DEBUGGING PURPOSE <=====================
	// ============================================================
	
	uint8_t last_data = data[data_len - 1];
	data[data_len - 1] = '\0';
	DEBUG("ble_ics_data_handler(): ->%d %d %s%c<-\r\n", data[0], 
																										  *((uint16_t*)(&data[1])), 
																											data[3], 
																											last_data);

	// ============================================================
	// ============================================================


}


