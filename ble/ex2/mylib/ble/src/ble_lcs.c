#include "ble_lcs.h"

// Function for handling the BLE_GAP_EVT_CONNECTED event
static void on_connect(ble_lcs_t * p_lcs, ble_evt_t * p_ble_evt)
{
    p_lcs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


// Function for handling the BLE_GAP_EVT_DISCONNECTED event
static void on_disconnect(ble_lcs_t * p_lcs, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_lcs->conn_handle = BLE_CONN_HANDLE_INVALID;
}



// Function for handling the BLE_GATTS_EVT_WRITE event
static void on_write(ble_lcs_t * p_lcs, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;


    if (p_evt_write->handle == p_lcs->led_state_handles.cccd_handle &&
  			p_evt_write->len == 2)
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
            p_lcs->is_notification_enabled = true;
        else
            p_lcs->is_notification_enabled = false;
    }
    else if (p_evt_write->handle == p_lcs->led_state_handles.value_handle &&
             p_lcs->data_handler != NULL)
    {
        p_lcs->data_handler(p_lcs, p_evt_write->data, p_evt_write->len);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}










// Function for updating state of LED
static void ble_lcs_led_state_update(uint8_t data)
{	
	static uint8_t is_LED_init = 0;

	DEBUG("ble_lcs_led_state_update(): %d\r\n", data);

	if(is_LED_init == 0)
	{
		init_LED1(); 
		is_LED_init = 1;
	}
	
	if(data)
		turnON_LED1();
	else
		turnOFF_LED1();
}








//  Function for adding LED state characteristic.
static uint32_t led_state_char_add(ble_lcs_t *p_lcs, const ble_lcs_init_t *p_lcs_init)
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		// ------------------------------------------------------------------------------------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;
		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		// -------------------------- GATT characteristic presentation format -----------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));

		pf_char_value.format = BLE_GATT_CPF_FORMAT_BOOLEAN;
		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------



		// ------------------------------ GATT characteristic metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		char_md.char_props.notify		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "LED custom service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------



		// ----------------------------------- GATT attribute metadata ------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_lcs_init->led_state_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_lcs_init->led_state_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------


		ble_uuid.type = p_lcs->uuid_type;
		ble_uuid.uuid = BLE_LCS_VALUE_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------
		// ------------------------------------------------------------------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);

		if(p_lcs_init->init_led_state == 0)
		{
			attr_char_value.p_value				=  (uint8_t *)"\x00";
			ble_lcs_led_state_update(0);
		}
		else
		{
			attr_char_value.p_value				= (uint8_t *)"\x01";	
			ble_lcs_led_state_update(1);
		}

		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------




		// ------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------

		err_code = sd_ble_gatts_characteristic_add(p_lcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_lcs->led_state_handles);


		return err_code;
}










void ble_lcs_on_ble_evt(ble_lcs_t * p_lcs, ble_evt_t * p_ble_evt)
{
    if(p_lcs == NULL || p_ble_evt == NULL)
    		return;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_lcs, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_lcs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_lcs, p_ble_evt);
            break;

        default:
          	break;
    }
}





uint32_t ble_lcs_init(ble_lcs_t * p_lcs, const ble_lcs_init_t * p_lcs_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t lcs_base_uuid = {BLE_LCS_UUID_BASE};

    VERIFY_PARAM_NOT_NULL(p_lcs);
    VERIFY_PARAM_NOT_NULL(p_lcs_init);

    // Initialize the service structure
    p_lcs->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_lcs->data_handler            = p_lcs_init->data_handler;
    p_lcs->is_notification_enabled = false;

    // Add a custom base UUID
    err_code = sd_ble_uuid_vs_add(&lcs_base_uuid, &p_lcs->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_lcs->uuid_type;
    ble_uuid.uuid = BLE_LCS_SERVICE_UUID;

    // Add the service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_lcs->service_handle);
    VERIFY_SUCCESS(err_code);

    // Add the LED state characteristic
    err_code = led_state_char_add(p_lcs, p_lcs_init);
    VERIFY_SUCCESS(err_code);


    return NRF_SUCCESS;
}






void ble_lcs_data_handler(ble_lcs_t *p_lcs, uint8_t *data, uint32_t data_len) 
{
	
	if(p_lcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_lcs_data_handler(): invalid params\r\n");
		return;
	}

	ble_lcs_led_state_update(data[0]);

	// ==============> FOR DEBUGGING PURPOSE <=====================
	// ============================================================
	
	uint8_t last_data = data[data_len - 1];
	data[data_len - 1] = '\0';
	DEBUG("ble_lcs_data_handler(): ->%s%c<-\r\n", data, last_data);

	// ============================================================
	// ============================================================


}






