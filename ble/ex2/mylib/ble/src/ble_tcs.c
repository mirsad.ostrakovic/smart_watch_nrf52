#include "ble_tcs.h"


static void on_connect(ble_tcs_t * p_tcs, ble_evt_t * p_ble_evt)
{
    p_tcs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}



static void on_disconnect(ble_tcs_t * p_tcs, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_tcs->conn_handle = BLE_CONN_HANDLE_INVALID;
}



static void on_write(ble_tcs_t * p_tcs, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

  	if(p_evt_write->handle == p_tcs->year_handles.value_handle)
    {
    	ble_tcs_year_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}
		else if(p_evt_write->handle == p_tcs->month_handles.value_handle)
		{
    	ble_tcs_month_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		} 
		else if(p_evt_write->handle == p_tcs->day_handles.value_handle)
		{
    	ble_tcs_day_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}	
		else if(p_evt_write->handle == p_tcs->wdu_handles.value_handle)
		{
    	ble_tcs_wdu_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}		
		else if(p_evt_write->handle == p_tcs->hour_handles.value_handle)
		{
    	ble_tcs_hour_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}		
		else if(p_evt_write->handle == p_tcs->minute_handles.value_handle)
		{
    	ble_tcs_minute_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}		
		else if(p_evt_write->handle == p_tcs->second_handles.value_handle)
		{
    	ble_tcs_second_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}
		else if(p_evt_write->handle == p_tcs->update_handles.value_handle)
		{
    	ble_tcs_update_handler(p_tcs, p_evt_write->data, p_evt_write->len); 
		}	
		else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}








void ble_tcs_on_ble_evt(ble_tcs_t * p_tcs, ble_evt_t * p_ble_evt)
{
    if (p_tcs == NULL || p_ble_evt == NULL)
        return;


    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_tcs, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_tcs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_tcs, p_ble_evt);
            break;

        default:
            // No implementation needed
            break;
    }
}








//  Function for adding year characteristic.
static uint32_t year_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "YEAR service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_YEAR_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->year_handles);


		return err_code;
}















//  Function for adding month characteristic.
static uint32_t month_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "MONTH service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_MONTH_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->month_handles);


		return err_code;
}




















//  Function for adding day characteristic.
static uint32_t day_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "DAY service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_DAY_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->day_handles);


		return err_code;
}












//  Function for adding WDU characteristic.
static uint32_t wdu_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "WDU service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_WDU_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->wdu_handles);


		return err_code;
}













//  Function for adding hour characteristic.
static uint32_t hour_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "HOUR service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_HOUR_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->hour_handles);


		return err_code;
}













//  Function for adding minute characteristic.
static uint32_t minute_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "MINUTE service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_MINUTE_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->minute_handles);


		return err_code;
}










//  Function for adding second characteristic.
static uint32_t second_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "SECOND service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_SECOND_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->second_handles);


		return err_code;
}








//  Function for adding update characteristic.
static uint32_t update_char_add(ble_tcs_t *p_tcs, const ble_tcs_init_t *p_tcs_init) 
{
		uint32_t err_code;
		ble_gatts_char_md_t 	char_md;
		ble_gatts_attr_md_t 	cccd_md;
		ble_gatts_attr_t			attr_char_value;
		ble_gatts_char_pf_t		pf_char_value;
		ble_uuid_t				  	ble_uuid;
		ble_gatts_attr_md_t 	attr_md;

		// -------------------- CCCD (Client Characteristic Configuration Descriptor) ---------------------
		memset(&cccd_md, 0, sizeof(cccd_md));

		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

		cccd_md.vloc = BLE_GATTS_VLOC_STACK;

		// -------------------------- GATT characteristic presentation format -----------------------------

		memset(&pf_char_value, 0, sizeof(pf_char_value));
		pf_char_value.format = BLE_GATT_CPF_FORMAT_UINT8;

		
		// ------------------------------ GATT characteristic metadata ------------------------------------
		memset(&char_md, 0, sizeof(char_md));	

		char_md.char_props.read			 = 1;
		char_md.char_props.write 		 = 1;
		
		// Pointer to a UTF-8 encoded string(not-NULL terminated), NULL if the descriptor in not required 
		char_md.p_char_user_desc     = NULL;
		
		// Pointer to a presentation format structure of NULL il the CPF descriptor in not required
		char_md.p_char_pf					   = &pf_char_value;	
		
		// Attribute metadata for the User Description descriptor, or NULL for default values
		char_md.p_user_desc_md			 = NULL; 
	
		// Attribute metadata for the Client Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_cccd_md						 = &cccd_md;
		
		// Attribute metadata for the Server Characteristic Configuration Descriptor, on NULL for default val
		char_md.p_sccd_md					   = NULL;

		// Add user description for characteristic
		char user_desc[] = "UPDATE service";

		char_md.p_char_user_desc = (uint8_t *)user_desc;
		char_md.char_user_desc_size = strlen(user_desc);
		char_md.char_user_desc_max_size = char_md.char_user_desc_size;


		// ----------------------------------- GATT attribute metadata ------------------------------------
		memset(&attr_md, 0, sizeof(attr_md));

		// Permission required for read operation(security mode: x, level: y)
		attr_md.read_perm 				= p_tcs_init->time_char_attr_md.read_perm;
		
		// Permission required for write operation(security mode: x, level: y)
		attr_md.write_perm 				= p_tcs_init->time_char_attr_md.write_perm;

		// Attribute Value is located on stack memory, no user memory is required
		attr_md.vloc							= BLE_GATTS_VLOC_STACK;
		
		// If rd_auth == 1, then read authorization and value will be requested from the app on every read op
		attr_md.rd_auth						= 0;
	
		// If wr_auth == 1, then write authorization will be requested from the app on every Write Request op
		attr_md.wr_auth						= 0;
	
		// Variable length argument, disabled
		attr_md.vlen							= 0;


		ble_uuid.type = p_tcs->uuid_type;
		ble_uuid.uuid = BLE_TCS_UPDATE_CHAR_UUID;

		// ---------------------------------------- GATT attribute ----------------------------------------

		memset(&attr_char_value, 0, sizeof(attr_char_value));

		// Pointer to attribute UUID
		attr_char_value.p_uuid 				= &ble_uuid;
		
		// Pointer to attribute metadata structure
		attr_char_value.p_attr_md 		= &attr_md;
		
		// Initial attribute value length in bytes
		attr_char_value.init_len			= sizeof(uint8_t);

		// Initial attribute value offset in bytes, if != 0 then, the first init_offs bytes will be uninit
		attr_char_value.init_offs			= 0;

		// Maximum attribute length(BLE_GATTS_FIX_ATTR_LEN_MAX == 510, BLE_GATTS_VAR_ATTR_LEN_MAX == 512)
		attr_char_value.max_len				= sizeof(uint8_t);



		err_code = sd_ble_gatts_characteristic_add(p_tcs->service_handle,
																							 &char_md,
																							 &attr_char_value,
																							 &p_tcs->update_handles);


		return err_code;
}













uint32_t ble_tcs_init(ble_tcs_t * p_tcs, const ble_tcs_init_t * p_tcs_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t tcs_base_uuid = {BLE_TCS_UUID_BASE};

    VERIFY_PARAM_NOT_NULL(p_tcs);
    VERIFY_PARAM_NOT_NULL(p_tcs_init);
		DEBUG("ble_tcs_init(): args not null\r\n");

    // Initialize the service structure
    p_tcs->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_tcs->is_notification_enabled = false;

    // Add a custom base UUID
    err_code = sd_ble_uuid_vs_add(&tcs_base_uuid, &p_tcs->uuid_type);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): sd_ble_uuid_vs_add succesffully finished\r\n");

    ble_uuid.type = p_tcs->uuid_type;
    ble_uuid.uuid = BLE_TCS_SERVICE_UUID;

    // Add the service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_tcs->service_handle);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): TIME update service created\r\n");

		
		err_code = year_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): YEAR char added\r\n");


		err_code = month_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): MONTH char added\r\n");


		err_code = day_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): DAY char added\r\n");


		err_code = wdu_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): WDU char added\r\n");


		err_code = hour_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): HOUR char added\r\n");


		err_code = minute_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): MINUTE char added\r\n");


		err_code = second_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): SECOND char added\r\n");


		err_code = update_char_add(p_tcs, p_tcs_init);
    VERIFY_SUCCESS(err_code);
		DEBUG("ble_tcs_init(): UPDATE char added\r\n");


    return NRF_SUCCESS;
}










void ble_tcs_year_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_year_handler(): invalid params\r\n");
		return;
	}


  write_register_STMcommunication(REGISTER_RTC_YEAR, data[0] % 100);
	DEBUG("ble_tcs_year_handler(): ->%d<-\r\n", data[0] % 100);
}




void ble_tcs_month_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_month_handler(): invalid params\r\n");
		return;
	}

	if(data[0] == 0) 
		data[0] = 1;

  write_register_STMcommunication(REGISTER_RTC_MONTH, data[0] % 13);
	DEBUG("ble_tcs_month_handler(): ->%d<-\r\n", data[0] % 13);
}




void ble_tcs_day_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_day_handler(): invalid params\r\n");
		return;
	}

	if(data[0] == 0) 
		data[0] = 1;

  write_register_STMcommunication(REGISTER_RTC_DAY, data[0] % 32);
	DEBUG("ble_tcs_day_handler(): ->%d<-\r\n", data[0] % 32);
}





void ble_tcs_wdu_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_wdu_handler(): invalid params\r\n");
		return;
	}

	if(data[0] == 0) 
		data[0] = 1;

  write_register_STMcommunication(REGISTER_RTC_WDU, data[0] % 8);
	DEBUG("ble_tcs_wdu_handler(): ->%d<-\r\n", data[0] % 8);
}





void ble_tcs_hour_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_hour_handler(): invalid params\r\n");
		return;
	}
	
  write_register_STMcommunication(REGISTER_RTC_HOUR, data[0] % 24);
	DEBUG("ble_tcs_hour_handler(): ->%d<-\r\n", data[0] % 24);
}




void ble_tcs_minute_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_minute_handler(): invalid params\r\n");
		return;
	}
	
  write_register_STMcommunication(REGISTER_RTC_MINUTE, data[0] % 60);
	DEBUG("ble_tcs_minute_handler(): ->%d<-\r\n", data[0] % 60);
}





void ble_tcs_second_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_second_handler(): invalid params\r\n");
		return;
	}
	
  write_register_STMcommunication(REGISTER_RTC_SECOND, data[0] % 60);
	DEBUG("ble_tcs_second_handler(): ->%d<-\r\n", data[0] % 60);
}





void ble_tcs_update_handler(ble_tcs_t *p_tcs, uint8_t *data, uint32_t data_len)
{

	if(p_tcs == NULL || data == NULL || data_len == 0)
	{
		DEBUG("ble_tcs_update_handler(): invalid params\r\n");
		return;
	}
	
  write_register_STMcommunication(REGISTER_RTC_COMMAND, 1);
	DEBUG("ble_tcs_update_handler(): ->%d<-\r\n", data[0]);
}
