#include "timer.h"

// ======================================= TIMER 1 ============================================
// ============================================================================================

static void (*TIMER1_IRQ_callback_)(void) = NULL;



void init_TIMER1()
{

	// (nRF52832 Product Specification v1.4, GPIO, page 239)
	// MODE   	<=> '00'b => Timer mode 
	NRF_TIMER1->MODE = TIMER_MODE_MODE_Timer; 

	// (nRF52832 Product Specification v1.4, GPIO, page 239)
	// BITMODE 	 <=> '00b' => 16 bit timer bit width	
	NRF_TIMER1->BITMODE = TIMER_BITMODE_BITMODE_16Bit; 

	// (nRF52832 Product Specification v1.4, GPIO, page 234)
	// f_TIMER = 16MHz / (2^PRESCALER) = 31250Hz
	NRF_TIMER1->PRESCALER = (9 << TIMER_PRESCALER_PRESCALER_Pos) & TIMER_PRESCALER_PRESCALER_Msk;  

	// (nRF52832 Product Specification v1.4, GPIO, page 240)
	// CC = 6250, Capute/Compare value set to 6250 => CC event will be generated every 6250 / f_TIMER = 0.2s
	NRF_TIMER1->CC[0] = 6250;  

	// (nRF52832 Product Specification v1.4, GPIO, page 236)
	// COMPARE0_CLEAR  <=> '1'b => Enable shortcut between COMPARE[0] event and CLEAR task
	NRF_TIMER1->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos) & TIMER_SHORTS_COMPARE0_CLEAR_Msk; 

	// (nRF52832 Product Specification v1.4, GPIO, page 237)
	// COMPARE0   <=> '1'b => Enable interrupt for COMPARE[0] event   	
	NRF_TIMER1->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos) & TIMER_INTENSET_COMPARE0_Msk;   

	// Enable interrupt for TIMER1
	NVIC_SetPriority(TIMER1_IRQn, 128);
	NVIC_EnableIRQ(TIMER1_IRQn);

	// Start TIMER1
 	NRF_TIMER1->TASKS_START = 1;
}


void set_TIMER1_IRQ_callback(void (*TIMER1_IRQ_callback)(void))
{
	TIMER1_IRQ_callback_ = TIMER1_IRQ_callback;  
}


void TIMER1_IRQHandler(void) 
{
	if(TIMER1_IRQ_callback_ != NULL)
		TIMER1_IRQ_callback_(); 

	// Cleare COMPARE[0] event flag, if it is not done, than app will lock in TIMER1_IRQHandler()
	NRF_TIMER1->EVENTS_COMPARE[0] = 0;
}


// ============================================================================================
// ============================================================================================
