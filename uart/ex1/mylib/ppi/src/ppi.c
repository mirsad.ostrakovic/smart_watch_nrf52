#include "ppi.h"


void configure_PPI(uint8_t channel, uint32_t event_reg_addr, uint32_t task_reg_addr)
{
	// (nRF52832 product specification v1.4, PPI, page 168)
	// Only channels 0-19 are configurable
	if(channel < 20)
	{
		// (nRF52832 product specification v1.4, PPI, page 178)
		NRF_PPI->CH[channel].EEP = event_reg_addr;
		NRF_PPI->CH[channel].TEP = task_reg_addr;
	}	
	
	// (nRF52832 Product Specification v1.4, PPI, page 173)
	// Enable channel 'channel'	
	NRF_PPI->CHENSET = (1 << channel); 
}
