#include "print.h"


void print_string(const char *str)
{

	while(*str != '\0')
		putchar(*(str++));
}



void print_number(long num)
{
	char buff[32];
	char *num_str;
	
	num_str = &buff[31];
	buff[0] = num >= 0 ? '+' : (num = -num, '-');
	*num_str = '\0';

	if(num == 0)
		*(--num_str) = '0';
	
	while(num != 0)
	{
		*(--num_str) = '0' + num % 10;
		num /= 10;
	}

	if(buff[0] == '-')
		*(--num_str) = '-';
	
	print_string(num_str);
}



void print_hex(long num, uint32_t type_size)
{
	char buff[type_size * 2 + 2];
	char *num_str;
	
	num_str = &buff[type_size * 2 + 1];
	buff[0] = num >= 0 ? '+' : (num = -num, '-');
	*num_str = '\0';

	while(num != 0)
	{
		*(--num_str) = (num % 16 < 10) ? ('0' + num % 16) : (num % 16 - 10 + 'A');
		num /= 16;
	}

	while(num_str > buff)
		*(--num_str) = '0';
	
	print_string(buff[0] == '-' ? &buff[0] : &buff[1]);
}





void print_binary(long num)
{
	char buff[128];
	char *num_str;
	
	num_str = &buff[31];
	buff[0] = num >= 0 ? '+' : (num = -num, '-');
	*num_str = '\0';

	if(num == 0)
		*(--num_str) = '0';

	while(num != 0)
	{
		*(--num_str) = '0' + num % 2;
		num /= 2;
	}

	if(buff[0] == '-')
		*(--num_str) = '-';
	
	print_string(num_str);
}



void print(const char *str, ...)
{
	
	uint8_t state = 0;
	//uint8_t substate = 0;

	va_list valist;
	va_start(valist, str);

	while(*str != '\0')
	{
		switch(state)	
		{
			// RAW CHARACTER state
			case 0: 
				if(*str != '%')		
					putchar(*str);
				else
					state = 1;
				++str;
				break;

			// INSERT OPERATOR START state
			case 1:
				switch(*(str++))
				{
					case 'd':
					case 'D':
						print_number(va_arg(valist, int));
						state = 0;
						break;

					case 'x':
					case 'X':
						if(*str == 'b' || *str == 'B')
						{
							print_binary(va_arg(valist, int));
							++str;
						}
						else
						{
							print_hex(va_arg(valist, int), sizeof(int));
							putchar(*(str++));
						}
						state = 0;
						break;
	
					case 's':
					case 'S':
							print_string(va_arg(valist, const char *));
							state = 0;
							break;
				}
				break;

		}
	}

	va_end(valist);
}


