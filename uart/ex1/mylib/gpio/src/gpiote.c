#include "gpiote.h"

// ----------------------------------------------------------
// (nRF52832 Development Kit v1.1.x User Guid v1.2 page 17)
// All LEDs are 'active low'
// ----------------------------------------------------------
void init_LED1_GPIOTE() 
{

	// (nRF52832 Product Specification v1.4, GPIO, page 162)
	// Configure GPIOTE as:
	// MODE   	<=> '11'b => Task mode, selected GPIO pin is selected as output, and will perform operation on it's value on SET, CRL or OUT task
	// PSEL   	<=> 'xxxxx'b => GPIO pin selected 
	// POLARITY <=> '11'b => Toggle mode, toggle pin from OUT[n] task
	// OUTINIT  <=> '0'b  => Output init value set as low, so LED1 will initially be TURN ON ()
	configure_GPIOTE(NRF52832_GPIOTE_LED1_CHANNEL, 
									 NRF52832_GPIO_LED1, 
									 GPIOTE_CONFIG_MODE_Task, 
									 GPIOTE_CONFIG_POLARITY_Toggle,
									 GPIOTE_CONFIG_OUTINIT_Low); 
}

