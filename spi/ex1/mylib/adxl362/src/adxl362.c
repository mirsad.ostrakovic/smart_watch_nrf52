#include "adxl362.h"

static uint8_t ADXL362_cs_pin;


void init_ADXL362(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint8_t cs_pin)
{

	ADXL362_cs_pin = cs_pin;

	configure_GPIO(ADXL362_cs_pin, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);

	set_GPIO_pin(ADXL362_cs_pin);

	init_SPIM0(sck_pin, mosi_pin, miso_pin, 
						 SPIM_FREQUENCY_4000000, SPIM_CPOL_ACTIVE_HIGH | SPIM_CPHA_LEADING | SPIM_ORDER_MSBFIRST);

	write_register_ADXL362(0x1F, 0x52);
	for(volatile int i = 0; i < 1000000; ++i);


	write_register_ADXL362(ADXL362_ACTIVITY_INACTIVITY_CTRL_REG, ADXL362_LOOP_MODE				   |
																															 ADXL362_INACTIVITY_DISABLED |
																															 ADXL362_ACTIVITY_DISABLED   | 
																															 ADXL362_INACTIVITY_REF_MODE | 
																															 ADXL362_ACTIVITY_REF_MODE	 ); 

	write_register_ADXL362(ADXL362_FILTER_CTRL_REG, ADXL362_RANGE_4G						|	  
																									ADXL362_HALF_BW_CLEAR       | 
																									ADXL362_EXT_SAMPLE_DISABLED |
																									ADXL362_ODR_12_5  					);
	

	write_register_ADXL362(ADXL362_POWER_CTRL_REG, ADXL362_EXT_CLK_DISABLED 		|  
																								 ADXL362_NOISE_LOW 						|
																								 ADXL362_WAKEUP_MODE_DISABLED |
																								 ADXL362_AUTOSLEEP_DISABLED   |
																								 ADXL362_MODE_MEASUREMENT     );


	write_register_ADXL362(ADXL362_FIFO_CTRL_REG, ADXL362_FIFO_MODE_OLDEST_SAVED 					|
																								ADXL362_FIFO_STORE_TEMPERATURE_DISABLED | 
																								ADXL362_FIFO_SAMPLES_MSB_1							);	


 	write_register_ADXL362(ADXL362_FIFO_SAMPLES_REG, 0x0); 

 	
	write_register_ADXL362(ADXL362_INTMAP1_REG, ADXL362_INTMAP1_FIFO_WATERMARK_INT);   


	print("init_ADXL362(): ADXL362_POWER_CTRL_REG: %x\r\n", read_register_ADXL362(ADXL362_POWER_CTRL_REG));
	print("init_ADXL362(): ADXL362_FILTER_CTRL_REG: %x\r\n", read_register_ADXL362(ADXL362_FILTER_CTRL_REG));
	print("init_ADXL362(): ADXL362_ACTIVITY_INACTIVITY_CTRL_REG: %x\r\n", read_register_ADXL362(ADXL362_ACTIVITY_INACTIVITY_CTRL_REG));
	print("init_ADXL362(): ADXL362_FIFO_CTRL_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_CTRL_REG));
	print("init_ADXL362(): ADXL362_FIFO_SAMPLES_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_SAMPLES_REG));
	print("init_ADXL362(): ADXL362_INTMAP1_REG: %x\r\n", read_register_ADXL362(ADXL362_INTMAP1_REG));
}


#define MIN(A,B)		(((A) > (B)) ? (B) : (A))


uint16_t read_FIFO_ADXL362(uint8_t receive_buff[], uint16_t receive_buff_len)
{
	uint8_t  transmit_buff[3] = { 0x0D , 0x0, 0x0 };
	uint16_t read_len;
	
	read_len = read_register_ADXL362(ADXL362_FIFO_ENTRIES_LOW_REG);	
	read_len |= read_register_ADXL362(ADXL362_FIFO_ENTRIES_HIGH_REG) << 8;		
	read_len = MIN(2 * read_len, receive_buff_len);
	read_len = 1024;

	clear_GPIO_pin(ADXL362_cs_pin);	
	send_data_SPIM0((char*)transmit_buff, 3, (char*)receive_buff, read_len);
	set_GPIO_pin(ADXL362_cs_pin);
	
	return read_len;
}



uint8_t read_register_ADXL362(uint8_t reg_addr)
{
	uint8_t transmit_buff[3] = { 0x0B , 0x0, 0x0 };
	uint8_t receive_buff[3] = { 0x00 , 0x0, 0x0 };

	transmit_buff[1] = reg_addr;

	clear_GPIO_pin(ADXL362_cs_pin);	
	send_data_SPIM0((char*)transmit_buff, 3, (char*)receive_buff, 3);
	set_GPIO_pin(ADXL362_cs_pin);

	return receive_buff[2];
}





void write_register_ADXL362(uint8_t reg_addr, uint8_t data)
{
	uint8_t transmit_buff[3] = { 0x0A , 0x0, 0x0 };
	uint8_t receive_buff[3] = { 0x00 , 0x0, 0x0 };

	transmit_buff[1] = reg_addr;
	transmit_buff[2] = data;

	clear_GPIO_pin(ADXL362_cs_pin);	
	send_data_SPIM0((char*)transmit_buff, 3, (char*)receive_buff, 3);
	set_GPIO_pin(ADXL362_cs_pin);
}






uint8_t check_ADXL362_status()
{

	if((read_register_ADXL362(ADXL362_DEVICE_ID_REG) != ADXL362_DEVICE_ID) 					 |
		 (read_register_ADXL362(ADXL362_MEMS_DEVICE_ID_REG) != ADXL362_MEMS_DEVICE_ID) |
		 (read_register_ADXL362(ADXL362_PART_ID_REG) != ADXL362_PART_ID)							 )
		return ADXL362_UNREACHABLE; 

	return ADXL362_AVAILABLE; 
}




uint16_t read_temperature_value_ADXL362()
{

	uint8_t temp_low = read_register_ADXL362(ADXL362_TEMPERATURE_LOW_REG);
	uint8_t temp_high = read_register_ADXL362(ADXL362_TEMPERATURE_HIGH_REG); 

	print("Temp hi: %d low: %d\r\n", temp_high, temp_low);

	return (temp_high << 8) | temp_low;
}



float ADXL362_temperature(uint16_t temperature)
{
	return (temperature >> 11 ? -1.0 : 1.0) * 
				 ((temperature & 0x7FF) * ADXL362_INV_TEMPERATURE_SCALE_FACTOR);  	
}
