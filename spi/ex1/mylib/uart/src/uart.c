#include "uart.h"


void init_UART(uint32_t baudrate, uint8_t tx_pin, uint8_t rx_pin)
{

	configure_GPIO(tx_pin,	
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Connect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);


	configure_GPIO(rx_pin,	
								 GPIO_PIN_CNF_DIR_Input,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);


	// Request to send information will not be used
	// (nRF52832 Product Specification v1.4, GPIO, page 537)
	// PSELRTS	<=> 'FFFFFFFF'x => Disconnected
	NRF_UART0->PSELRTS = UART_PSELRTS_PSELRTS_Disconnected;


	// (nRF52832 Product Specification v1.4, GPIO, page 537)
	// PSELTXD <=> [0..31] => Pin number configuration for UART TXD signal
	NRF_UART0->PSELTXD = tx_pin & 0x1F;


	// Clear to Send information will not be used
	// (nRF52832 Product Specification v1.4, GPIO, page 537 and page 538)	
	// PSELCTS <=> 'FFFFFFFF'x => Disconnected	
	NRF_UART0->PSELCTS = UART_PSELCTS_PSELCTS_Disconnected;  


	// (nRF52832 Product Specification v1.4, GPIO, page 538)	
	// PSELRXD <=> [0..31] => Pin number configuration for UART RXD signal	
	NRF_UART0->PSELRXD = rx_pin & 0x1F;  


	// (nrf52832 product specification v1.4, gpio, page 538)	
	// BAUDRATE <=> for every baud rate specific value is defined in the manual 	
	NRF_UART0->BAUDRATE = baudrate;


	// (nRF52832 Product Specification v1.4, GPIO, page 534)
	// If hardware flow control is disabled then CTS and RTS will not be used
	// Parity by default is 'even parity'
	// (nRF52832 Product Specification v1.4, GPIO, page 539)
	// HWFC   <=> Hardware flow control => Disabled
	// PARITY <=> Parity => Included parity bit
	NRF_UART0->CONFIG = UART_CONFIG_PARITY_Included | UART_CONFIG_HWFC_Disabled;  


	// (nRF52832 Product Specification v1.4, GPIO, page 537)
	// ENABLE   	<=> '0100'b => Enabled 
	NRF_UART0->ENABLE = UART_ENABLE_ENABLE_Enabled; 


	// Generate task to start RX component
	NRF_UART0->TASKS_STARTRX = 1;


	// (nRF52832 Product Specification v1.4, GPIO, page 533)
	// To secure that the CPU can detect all incoming RXDRDY events through the RXDRDY
	// event register, the RXDRDY event register must be cleared before the RXD register is READ
	NRF_UART0->EVENTS_RXDRDY = 0;


	// To remove 'wild character' on first print 
	NRF_UART0->TXD = '\r';
	
	// Generate task to start TX component
	NRF_UART0->TASKS_STARTTX = 1;

	// To remove 'wild characted' on first print
	putchar_UART('\r');
}



void putchar_UART(uint8_t data)
{
	NRF_UART0->TXD = data;

	// (nRF52832 Product Specification v1.4, GPIO, page 532)
	// When a byte has been successfully trasmitted the UART wil generate a TXDRDY event after
	// which a new byte can be written to the TXD register
	while (NRF_UART0->EVENTS_TXDRDY != 1);
	NRF_UART0->EVENTS_TXDRDY = 0;
}
