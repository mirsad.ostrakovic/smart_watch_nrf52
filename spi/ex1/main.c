#include <nrf52.h>
#include "gpio.h"
#include "gpiote.h"
#include "timer.h"
#include "ppi.h"
#include "print.h"
#include "adxl362.h"



void toggle_LED1()
{
	static char state = 0;

	if(state == 0)
	{
		turnOFF_LED1();
		state = 1;	
	}
	else 
	{
		turnON_LED1();
		state = 0;	
	}
}


static volatile uint8_t TIMER1_flag = 0x0;

void TIMER1_callback()
{	
	TIMER1_flag = 0x1;
	print("TIMER1_message(): called\r\n");
	
	// Manual generate event for GPIOTE peripheral, which will toggle pin value	
	// NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL] = 1;
}


int main(void)
{

		init_PRINT();
		print("init_PRINT(): compleated\r\n");
		
		init_TIMER1();		
		print("init_TIMER1(): compleated\r\n");
		
		//init_LED1();  	
		//print("init_LED1(): compleated\r\n");
		
		//init_LED1_GPIOTE(); 
		//print("init_LED1_GPIOTE(): compleated\r\n");

		init_ADXL362(ADXL362_SCK_PIN, ADXL362_MOSI_PIN, ADXL362_MISO_PIN, ADXL362_CS_PIN);
		print("init_ADXL362(): compleated\r\n");

		if(check_ADXL362_status() != ADXL362_AVAILABLE)
		{
			print("check_ADXL362_status(): ADXL362 is not available\r\n");
		}

		set_TIMER1_IRQ_callback(TIMER1_callback);

		// Set PPI(Programmable peripheral interconnection) to connect TIMER1 COMPARE[0] event to NRF_GPIOTE TASK_OUT for LED1_CHANNEL
		//configure_PPI(NRF52832_PPI_CHANNEL_0, (uint32_t)&(NRF_TIMER1->EVENTS_COMPARE[0]), (uint32_t)&(NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL]));


		//init_ADXL362_INT1_pin();
		//print("init_ADXL362_INT1_pin()\r\n");	
		init_GPIOTE_ADXL362_INT1_interrupt();
		print("init_GPIOTE_ADXL362_INT1_interrupt()\r\n");	



		while (1)
		{

			if(get_ADXL362_INT1_flag() != 0)
			{	
				uint8_t ADXL362_data_buff[1024];
				uint16_t ADXL362_data_buff_sz = 1024;

				clear_ADXL362_INT1_flag();
				ADXL362_data_buff_sz = read_FIFO_ADXL362(ADXL362_data_buff, ADXL362_data_buff_sz);
				print("FIFO read size: %d\r\n", ADXL362_data_buff_sz);	

				print("--> ADXL362_STATUS_REG: %x\r\n", read_register_ADXL362(ADXL362_STATUS_REG));
				print("--> ADXL362_FIFO_ENTRIES_LOW_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_LOW_REG));		
				print("--> ADXL362_FIFO_ENTRIES_HIGH_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_HIGH_REG));	
				

				if(NRF_P0->IN & (1 << NRF52832_GPIO_ADXL362_INT1))
					print("----> ADXL362 INT is SET\r\n");	
				else
					print("----> ADXL362 INT is CLEARED\r\n");	

				for(int i = 0; i < ADXL362_data_buff_sz; i += 2)
				{
					print("%c[%d]: %c%d\r\n",  'X' + ((ADXL362_data_buff[i+1] >> 6) & 0x3), 
																	i / 2,
																	((ADXL362_data_buff[i+1] >> 4) & 0x1) ? '-' : '+',
																	((ADXL362_data_buff[i+1] & 0xF) << 8) | ADXL362_data_buff[i]);
				}

				print("ADXL362_INT1_flag is SET\r\n");
			}
		
			//print("NRF_GPIOTE->INTENSET: %x\r\n", NRF_GPIOTE->INTENSET);  
		//	print("NRF_GPIOTE->EVENTS_IN[NRF52832_GPIOTE_ADXL362_INT1_CHANNEL] = %d\r\n", NRF_GPIOTE->EVENTS_IN[NRF52832_GPIOTE_ADXL362_INT1_CHANNEL]);

			
			if(NRF_P0->IN & (1 << NRF52832_GPIO_ADXL362_INT1))
			{
				print("ADXL362 INT is SET\r\n");	
				print("ADXL362_FIFO_ENTRIES_LOW_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_LOW_REG));		
				print("ADXL362_FIFO_ENTRIES_HIGH_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_HIGH_REG));
			}
			else
			{
				print("ADXL362 INT is CLEARED\r\n");
				print("ADXL362_FIFO_ENTRIES_LOW_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_LOW_REG));		
				print("ADXL362_FIFO_ENTRIES_HIGH_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_HIGH_REG));
			}
			if(TIMER1_flag != 0x0)
			{		

				/*
				print("ID: %x\r\n", read_register_ADXL362(0x0));	
				print("MEM ID: %x\r\n", read_register_ADXL362(0x1));	
				print("PART ID: %x\r\n", read_register_ADXL362(0x2));	

				print("ADXL362 temperature: %f\r\n", ADXL362_temperature(read_temperature_value_ADXL362()));
				print("ADXL362_STATUS_REG: %x\r\n", read_register_ADXL362(0x0B));
				print("ADXL362_FIFO_CTRL_REG: %x\r\n", read_register_ADXL362(0x28));
				*/

				//print("ADXL362_FIFO_ENTRIES_LOW_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_LOW_REG));		
				//print("ADXL362_FIFO_ENTRIES_HIGH_REG: %x\r\n", read_register_ADXL362(ADXL362_FIFO_ENTRIES_HIGH_REG));

				TIMER1_flag = 0x0;
				
				//print("X: %d\r\n", read_register_ADXL362(0x8));	
				//print("Y: %d\r\n", read_register_ADXL362(0x9));
				//print("Z: %d\r\n", read_register_ADXL362(0xA));
			}	
		}

}


