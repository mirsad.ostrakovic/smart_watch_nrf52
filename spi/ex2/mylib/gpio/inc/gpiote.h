#ifndef GPIOTE_H_
#define GPIOTE_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include "gpio.h"
#include "print.h"




// (nRF52832 Product Specification v1.4, GPIO, page 162)
// configure_GPIO() MACRO can be called as function
#define configure_GPIOTE(channel, pin, mode, polarity, initval)  \
do{															 																 \
	NRF_GPIOTE->CONFIG[(uint32_t)channel] = ((((uint32_t)(initval)) << GPIOTE_CONFIG_OUTINIT_Pos) & GPIOTE_CONFIG_OUTINIT_Msk) 		| \
																					((((uint32_t)(polarity)) << GPIOTE_CONFIG_POLARITY_Pos) & GPIOTE_CONFIG_POLARITY_Msk) | \
																					((((uint32_t)(pin)) << GPIOTE_CONFIG_PSEL_Pos) & GPIOTE_CONFIG_PSEL_Msk) 							| \
																					((((uint32_t)(mode)) << GPIOTE_CONFIG_MODE_Pos) & GPIOTE_CONFIG_MODE_Msk); 							\
} while(0)








// ====================================== LED ============================================
// =======================================================================================

#define NRF52832_GPIOTE_LED1_CHANNEL  	0
#define NRF52832_GPIOTE_LED2_CHANNEL    1	
#define NRF52832_GPIOTE_LED3_CHANNEL    2	
#define NRF52832_GPIOTE_LED4_CHANNEL		3

void init_LED1_GPIOTE(); 


// =============================== ADXL362 INTERRUPT =====================================
// =======================================================================================

#define NRF52832_GPIOTE_ADXL362_INT1_CHANNEL  	2
#define NRF52832_GPIOTE_ADXL362_INT2_CHANNEL    3	

void init_GPIOTE_ADXL362_INT1_interrupt();

uint8_t get_ADXL362_INT1_flag();
void clear_ADXL362_INT1_flag();

#endif
