#ifndef TIMER_H_
#define TIMER_H_

#include <stddef.h>
#include <nrf52.h>
#include <nrf52_bitfields.h>


void init_TIMER1();
void set_TIMER1_IRQ_callback(void (*TIMER1_IRQ_callback)(void));

#endif
