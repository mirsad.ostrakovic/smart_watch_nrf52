#include "spim.h"


void init_SPIM0(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint32_t freq, uint32_t config_flags)
{

	configure_GPIO(sck_pin, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Disabled,
        				 GPIO_PIN_CNF_DRIVE_H0H1,
        				 GPIO_PIN_CNF_SENSE_Disabled);


	configure_GPIO(mosi_pin, 
								 GPIO_PIN_CNF_DIR_Output,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Pullup,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);


	configure_GPIO(miso_pin, 
								 GPIO_PIN_CNF_DIR_Input,
        				 GPIO_PIN_CNF_INPUT_Disconnect,
        				 GPIO_PIN_CNF_PULL_Pullup,
        				 GPIO_PIN_CNF_DRIVE_S0S1,
        				 GPIO_PIN_CNF_SENSE_Disabled);


	// (nRF52832 Product Specification v1.4, SPIM, page 287)
	// PIN     <=> 'xxxxx'b => pin for SPI SCK 	
	// CONNECT <=> '0'b => connect
	NRF_SPIM0->PSEL.SCK = ((sck_pin << SPIM_PSEL_SCK_PIN_Pos) & SPIM_PSEL_SCK_PIN_Msk) | SPIM_PSEL_SCK_CONNECT_Connected;  


	// (nRF52832 Product Specification v1.4, SPIM, page 287)
	// PIN     <=> 'xxxxx'b => pin for SPI MOSI	
	// CONNECT <=> '0'b => connect
	NRF_SPIM0->PSEL.MOSI = ((mosi_pin << SPIM_PSEL_MOSI_PIN_Pos) & SPIM_PSEL_MOSI_PIN_Msk) | SPIM_PSEL_MOSI_CONNECT_Connected;  


	// (nRF52832 Product Specification v1.4, SPIM, pages 287 and 288)
	// PIN     <=> 'xxxxx'b => pin for SPI MISO 	
	// CONNECT <=> '0'b => connect
	NRF_SPIM0->PSEL.MISO = ((miso_pin << SPIM_PSEL_MISO_PIN_Pos) & SPIM_PSEL_MISO_PIN_Msk) | SPIM_PSEL_MISO_CONNECT_Connected;  


	// (nRF52832 Product Specification v1.4, SPIM, page 288)
	// FREQUENCY <=> 'xxxxxxxx'x => possible values are specified in the manual	
	NRF_SPIM0->FREQUENCY = freq;


	// (nRF52832 Product Specification v1.4, SPIM, page 290)
	// ORDER <=> 'x'b => MSB/LSB[0/1] bit shifted out first
	// CPHA	 <=> 'x'b => SCK phase leading/trailing[0/1]
	// CPOL  <=> 'x'b => SCK polarity active high/low[0/1]
	NRF_SPIM0->CONFIG = config_flags;


	// (nRF52832 Product Specification v1.4, SPIM, page 290)
	// ORC <=> 'xxxxxxxx'b => character clocked out in case of overread of the transmit buffer	
	NRF_SPIM0->ORC = 0x0;


	// (nRF52832 Product Specification v1.4, SPIM, page 287)
	// ENABLE <=> '0111'b => SPIM enabled
	NRF_SPIM0->ENABLE = SPIM_ENABLE_ENABLE_Enabled;  
}


void config_SPIM0_receive_buffer(char *receive_buff, uint32_t receive_buff_len)
{
	// (nRF52832 Product Specification v1.4, SPIM, page 288)
	// PTR  <=> 'xxxxxxxx'x => pointer to read	
	NRF_SPIM0->RXD.PTR = (uint32_t)receive_buff;

	// (nRF52832 Product Specification v1.4, SPIM, page 288)
	// MAXCNT => '000000xx'x => maximum number of byte to receive
	NRF_SPIM0->RXD.MAXCNT = (receive_buff_len << SPIM_RXD_MAXCNT_MAXCNT_Pos) & SPIM_RXD_MAXCNT_MAXCNT_Msk ;

	// (nRF52832 Product Specification v1.4, SPIM, page 289)
	// LIST => '0b' => Disabled EasyDMA list
	//NRF_SPIM0->RXD.LIST = SPIM_RXD_LIST_LIST_Disabled;
	NRF_SPIM0->RXD.LIST = SPIM_RXD_LIST_LIST_ArrayList;
}


void config_SPIM0_transmit_buffer(char *transmit_buff, uint32_t transmit_buff_len)
{
	// (nRF52832 Product Specification v1.4, SPIM, page 288)
	// PTR  <=> 'xxxxxxxx'x => pointer to read	
	NRF_SPIM0->TXD.PTR = (uint32_t)transmit_buff;

	// (nRF52832 Product Specification v1.4, SPIM, page 288)
	// MAXCNT => '000000xx'x => maximum number of byte to receive
	NRF_SPIM0->TXD.MAXCNT = (transmit_buff_len << SPIM_TXD_MAXCNT_MAXCNT_Pos) & SPIM_TXD_MAXCNT_MAXCNT_Msk ;

	// (nRF52832 Product Specification v1.4, SPIM, page 289)
	// LIST => '0b' => Disabled EasyDMA list
	//NRF_SPIM0->TXD.LIST = SPIM_TXD_LIST_LIST_Disabled;
	NRF_SPIM0->TXD.LIST = SPIM_TXD_LIST_LIST_ArrayList;
}


uint32_t send_data_SPIM0(char *tx_buff, uint32_t tx_buff_len, char *rx_buff, uint32_t rx_buff_len)
{
	config_SPIM0_transmit_buffer(tx_buff, tx_buff_len);
	config_SPIM0_receive_buffer(rx_buff, rx_buff_len);

	// Clear TXD and RXD amount
	// Clear END of transaction EVENT flag
	NRF_SPIM0->EVENTS_END = 0;

	// Generate START TASK
	NRF_SPIM0->TASKS_START = 1;

	// Wait for transaction to complete
	while(NRF_SPIM0->EVENTS_END != 1);

	return SEND_DATA_RETURN_VALUE(NRF_SPIM0->TXD.AMOUNT, NRF_SPIM0->RXD.AMOUNT);
}
