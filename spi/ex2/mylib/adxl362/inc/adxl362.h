#ifndef ADXL362_H_
#define ADXL362_H_

#include <nrf52.h>
#include <nrf52_bitfields.h>
#include "spim.h"

// ============= ADXL362 PIN CONFIGURATION ==============
// ======================================================

#define ADXL362_SCK_PIN 	16
#define ADXL362_MOSI_PIN  18 
#define ADXL362_MISO_PIN  17
#define ADXL362_CS_PIN    19



// =============== ADXL362 DEVICE INFO ==================
// ======================================================

#define ADXL362_DEVICE_ID_REG 						0x0
#define ADXL362_MEMS_DEVICE_ID_REG				0x1
#define ADXL362_PART_ID_REG					  		0x2
#define ADXL362_SILICON_REVISION_ID_REG 	0x3

#define ADXL362_DEVICE_ID  						0xAD
#define ADXL362_MEMS_DEVICE_ID			  0x1D
#define ADXL362_PART_ID								0xF2



// ============== ADXL362 STATUS REGISTER ===============
// ======================================================

#define ADXL362_STATUS_REG 						0xB


// =========== ADXL362 FIFO ENTRIES REGISTER ============
// ======================================================

#define ADXL362_FIFO_ENTRIES_LOW_REG		0xC
#define ADXL362_FIFO_ENTRIES_HIGH_REG		0xD


// =========== ADXL362 ACTIVITY TRESHOLD REGISTER ============
// ===========================================================

#define ADXL362_ACTIVITY_TRESHOLD_LOW_REG 	0x20
#define ADXL362_ACTIVITY_TRESHOLD_HIGH_REG 	0x21


// =========== ADXL362 ACTIVITY TIME REGISTER ============
// =======================================================

#define ADXL362_ACTIVITY_TIME_REG 	0x22


// =========== ADXL362 INACTIVITY TRESHOLD REGISTER ============
// ===========================================================

#define ADXL362_INACTIVITY_TRESHOLD_LOW_REG 	0x23
#define ADXL362_INACTIVITY_TRESHOLD_HIGH_REG 	0x24


// =========== ADXL362 INACTIVITY TIME REGISTER ============
// =======================================================

#define ADXL362_INACTIVITY_TIME_LOW_REG 	0x25
#define ADXL362_INACTIVITY_TIME_HIGH_REG 	0x26


// =========== ADXL362 ACTIVITY/INACTIVITY CONTROL REGISTER ============
// =====================================================================

#define ADXL362_ACTIVITY_INACTIVITY_CTRL_REG 		0x27	


#define ADXL362_DEFAULT_MODE 		0x00 
#define ADXL362_LINKED_MODE 		0x10 
#define ADXL362_LOOP_MODE 			0x30	


#define ADXL362_INACTIVITY_REF_MODE 0x8
#define ADXL362_INACTIVITY_ABS_MODE 0x0


#define ADXL362_INACTIVITY_ENABLE   0x4
#define ADXL362_INACTIVITY_DISABLED 0x0


#define ADXL362_ACTIVITY_REF_MODE 	0x2
#define ADXL362_ACTIVITY_ABS_MODE 	0x0


#define ADXL362_ACTIVITY_ENABLE   	0x1
#define ADXL362_ACTIVITY_DISABLED 	0x0


// =========== ADXL362 FIFO CONTROL REGISTER ============
// ======================================================

#define ADXL362_FIFO_CTRL_REG 		0x28	

#define ADXL362_FIFO_MODE_DISABLED 		 	0x0
#define ADXL362_FIFO_MODE_OLDEST_SAVED  0x1
#define ADXL362_FIFO_MODE_STREAM			  0x2
#define ADXL362_FIFO_MODE_TRIGGERED			0x3


#define ADXL362_FIFO_STORE_TEMPERATURE_DISABLED 0x0
#define ADXL362_FIFO_STORE_TEMPERATURE_ENABLED 	0x4

#define ADXL362_FIFO_SAMPLES_MSB_1	0x8
#define ADXL362_FIFO_SAMPLES_MSB_0	0x0


// =========== ADXL362 FIFO SAMPLES REGISTER ============
// ======================================================

#define ADXL362_FIFO_SAMPLES_REG 		0x29	



// ============== ADXL362 INTMAP1 REGISTER ==============
// ======================================================

#define ADXL362_INTMAP1_REG 					0x2A

#define ADXL362_INTMAP1_DATA_READY_INT			0x1
#define ADXL362_INTMAP1_FIFO_READY_INT 	  	0x2
#define ADXL362_INTMAP1_FIFO_WATERMARK_INT 	0x4
#define ADXL362_INTMAP1_FIFO_OVERRUN_INT 	  0x8
#define ADXL362_INTMAP1_ACT_INT 	     			0x10
#define ADXL362_INTMAP1_INACT_INT 	    		0x20
#define ADXL362_INTMAP1_AWAKE_INT		 	 		  0x40
#define ADXL362_INTMAP1_SET_INT_ACTIVE_LOW	0x80




// ============== ADXL362 INTMAP2 REGISTER ==============
// ======================================================

#define ADXL362_INTMAP2_REG 					0x2B

#define ADXL362_INTMAP2_DATA_READY_INT			0x1
#define ADXL362_INTMAP2_FIFO_READY_INT 	  	0x2
#define ADXL362_INTMAP2_FIFO_WATERMARK_INT 	0x4
#define ADXL362_INTMAP2_FIFO_OVERRUN_INT 	  0x8
#define ADXL362_INTMAP2_ACT_INT 	     			0x10
#define ADXL362_INTMAP2_INACT_INT 	    		0x20
#define ADXL362_INTMAP2_AWAKE_INT		 	 		  0x40
#define ADXL362_INTMAP2_SET_INT_ACTIVE_LOW	0x80



// =========== ADXL362 FILTER CONTROL REGISTER ============
// ========================================================

#define ADXL362_FILTER_CTRL_REG 		0x2C	


#define ADXL362_RANGE_2G		0x00
#define ADXL362_RANGE_4G		0x40
#define ADXL362_RANGE_8G		0x80


#define ADXL362_HALF_BW_SET   0x10
#define ADXL362_HALF_BW_CLEAR 0x00


#define ADXL362_EXT_SAMPLE_ENABLED 0x8
#define ADXL362_EXT_SAMPLE_DISABLED 0x0


#define ADXL362_ODR_12_5 		0x0
#define ADXL362_ODR_25 		  0x1
#define ADXL362_ODR_50 		  0x2
#define ADXL362_ODR_100 		0x3
#define ADXL362_ODR_200 		0x4
#define ADXL362_ODR_400 		0x5



// =========== ADXL362 POWER CONTROL REGISTER ============
// ========================================================


#define ADXL362_POWER_CTRL_REG 		 0x2D	


#define ADXL362_EXT_CLK_ENABLED  			0x40
#define ADXL362_EXT_CLK_DISABLED  		0x00


#define ADXL362_NOISE_NORMAL  			  0x00
#define ADXL362_NOISE_LOW  			  		0x10
#define ADXL362_NOISE_ULTRALOW  			0x20


#define ADXL362_WAKEUP_MODE_ENABLED   0x8
#define ADXL362_WAKEUP_MODE_DISABLED  0x0


#define ADXL362_AUTOSLEEP_ENABLED  		0x4
#define ADXL362_AUTOSLEEP_DISABLED 		0x0


#define ADXL362_MODE_STANDBY 			 		0x0
#define ADXL362_MODE_MEASUREMENT 	 		0x2


// ========== CHECK IS ADXL362 AVAILABLE STATUS =========
// ======================================================

#define ADXL362_AVAILABLE 		0x0
#define ADXL362_UNREACHABLE 	0x1

uint8_t check_ADXL362_status();


// ============ ADXL362 PRIMITIVE FUNCTIONS =============
// ======================================================

void init_ADXL362(uint8_t sck_pin, uint8_t mosi_pin, uint8_t miso_pin, uint8_t cs_pin);

void write_register_ADXL362(uint8_t reg_addr, uint8_t data);

uint16_t read_FIFO_ADXL362(uint8_t receive_buff[], uint16_t receive_buff_len);
uint8_t read_register_ADXL362(uint8_t reg_addr);
void read_registers_ADXL362(uint8_t reg_addr, uint8_t read_buff[], uint8_t read_len);
// =================== ADXL362 TEMPERATURE ===============
// =======================================================

#define ADXL362_TEMPERATURE_LOW_REG 	0x14
#define ADXL362_TEMPERATURE_HIGH_REG 	0x15

#define ADXL362_INV_TEMPERATURE_SCALE_FACTOR 0.625

uint16_t read_temperature_value_ADXL362();
float ADXL362_temperature(uint16_t temperature);

#endif
