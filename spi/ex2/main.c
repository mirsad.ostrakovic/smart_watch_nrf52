#include <nrf52.h>
#include "gpio.h"
#include "gpiote.h"
#include "timer.h"
#include "ppi.h"
#include "print.h"
#include "adxl362.h"

#include <math.h>


void toggle_LED1()
{
	static char state = 0;

	if(state == 0)
	{
		turnOFF_LED1();
		state = 1;	
	}
	else 
	{
		turnON_LED1();
		state = 0;	
	}
}


static volatile uint8_t TIMER1_flag = 0x0;

void TIMER1_callback()
{	
	TIMER1_flag = 0x1;
	print("TIMER1_message(): called\r\n");
	
	// Manual generate event for GPIOTE peripheral, which will toggle pin value	
	// NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL] = 1;
}


int main(void)
{

		init_PRINT();
		print("init_PRINT(): compleated\r\n");
		
		init_TIMER1();		
		print("init_TIMER1(): compleated\r\n");
		
		//init_LED1();  	
		//print("init_LED1(): compleated\r\n");
		
		//init_LED1_GPIOTE(); 
		//print("init_LED1_GPIOTE(): compleated\r\n");

		init_ADXL362(ADXL362_SCK_PIN, ADXL362_MOSI_PIN, ADXL362_MISO_PIN, ADXL362_CS_PIN);
		print("init_ADXL362(): compleated\r\n");

		if(check_ADXL362_status() != ADXL362_AVAILABLE)
		{
			print("check_ADXL362_status(): ADXL362 is not available\r\n");
		}

		set_TIMER1_IRQ_callback(TIMER1_callback);

		// Set PPI(Programmable peripheral interconnection) to connect TIMER1 COMPARE[0] event to NRF_GPIOTE TASK_OUT for LED1_CHANNEL
		//configure_PPI(NRF52832_PPI_CHANNEL_0, (uint32_t)&(NRF_TIMER1->EVENTS_COMPARE[0]), (uint32_t)&(NRF_GPIOTE->TASKS_OUT[NRF52832_GPIOTE_LED1_CHANNEL]));


		//init_ADXL362_INT1_pin();
		//print("init_ADXL362_INT1_pin()\r\n");	
		//init_GPIOTE_ADXL362_INT1_interrupt();
		//print("init_GPIOTE_ADXL362_INT1_interrupt()\r\n");	


		float pedometer_x_accel_avg = read_register_ADXL362(0x8) / 32.0;
		float pedometer_y_accel_avg = read_register_ADXL362(0x9) / 32.0;
		float pedometer_z_accel_avg = read_register_ADXL362(0xA) / 32.0;


		while (1)
		{
	
			
			




			if(TIMER1_flag != 0x0)
			{		
				float x_accel_f, y_accel_f, z_accel_f;		
				float x_accel_eff, y_accel_eff, z_accel_eff;	
				int8_t x_accel, y_accel, z_accel;

				x_accel = read_register_ADXL362(0x8);
				y_accel = read_register_ADXL362(0x9);
				z_accel = read_register_ADXL362(0xA);
					
				x_accel_f = x_accel / 32.0;
				y_accel_f = y_accel / 32.0;
				z_accel_f = z_accel / 32.0;
	
				pedometer_x_accel_avg = 0.9 * pedometer_x_accel_avg + 0.1 * x_accel_f;
				pedometer_y_accel_avg = 0.9 * pedometer_y_accel_avg + 0.1 * y_accel_f;
				pedometer_z_accel_avg = 0.9 * pedometer_z_accel_avg + 0.1 * z_accel_f;

				print("current_x: %f\r\ncurrent_y: %f\r\ncurrent_z: %f\r\n", x_accel_f, y_accel_f, z_accel_f);			
			
		
				print("x_avg: %f\r\ny_avg: %f\r\nz_avg: %f\r\n", pedometer_x_accel_avg, 
																												 pedometer_y_accel_avg, 
				  																							 pedometer_z_accel_avg);
			
				x_accel_eff = x_accel_f - pedometer_x_accel_avg; 
				y_accel_eff = y_accel_f - pedometer_y_accel_avg; 
				z_accel_eff = z_accel_f - pedometer_z_accel_avg; 
	
			
				print("x_eff: %f\r\ny_eff: %f\r\nz_eff: %f\r\n", x_accel_eff, y_accel_eff, z_accel_eff);			
		
				float pedometar_scalar_product = x_accel_eff * pedometer_x_accel_avg +  
																				 y_accel_eff * pedometer_y_accel_avg + 
																				 z_accel_eff * pedometer_z_accel_avg;

				print("pedometar_scala_product: %f\r\n\r\n\r\n", pedometar_scalar_product);

				TIMER1_flag = 0x0;
			}	
		}

}


